import { AutoMap } from '@automapper/classes';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Brand } from './Brand';
import { Category } from './Category';
import { FavouriteProduct } from './FavouriteProduct';
import { ProductImage } from './ProductImage';
import { Variant } from './Variant';

@Entity({
  name: 'product',
})
export class Product {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  @AutoMap()
  id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 300,
    nullable: false,
  })
  @AutoMap()
  name: string;

  @Column({
    name: 'slug',
    type: 'varchar',
    length: 300,
    nullable: false,
    unique: true,
  })
  @AutoMap()
  slug: string;

  @Column({
    name: 'short_description',
    type: 'varchar',
    length: 300,
    nullable: false,
  })
  @AutoMap()
  shortDescription: string;

  @Column({
    name: 'description',
    type: 'text',
    nullable: false,
  })
  @AutoMap()
  description: string;

  @Column({
    name: 'favourite_count',
    type: 'int',
    nullable: false,
    default: 0,
  })
  @AutoMap()
  favouriteCount: number;

  @Column({
    name: 'rate_count',
    type: 'int',
    nullable: false,
    default: 0,
  })
  @AutoMap()
  rateCount: number;

  @Column({
    name: 'average_rate',
    type: 'float',
    nullable: false,
    default: 0,
  })
  @AutoMap()
  averageRate: number;

  @Column({
    name: 'is_active',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  @AutoMap()
  isActive: boolean;

  @Column({
    name: 'is_valid',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  @AutoMap()
  isValid: boolean;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  @AutoMap()
  createdAt: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  @AutoMap()
  updatedAt: Date;

  @ManyToOne(() => Brand, (brand) => brand.products, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'brand_id',
  })
  @AutoMap(() => Brand)
  brand: Brand;

  @OneToMany(() => Variant, (variant) => variant.product, {
    cascade: ['insert'],
  })
  @AutoMap(() => [Variant])
  variants: Variant[];

  @OneToMany(
    () => FavouriteProduct,
    (favouriteProduct) => favouriteProduct.product,
    {
      cascade: ['insert'],
    }
  )
  favouriteProducts: FavouriteProduct[];

  @OneToMany(() => ProductImage, (productImage) => productImage.product, {
    cascade: ['insert'],
  })
  @AutoMap(() => [ProductImage])
  productImages: ProductImage[];

  @ManyToMany(() => Category, {
    cascade: ['insert'],
  })
  @JoinTable({
    name: 'product_category',
    joinColumn: {
      name: 'product_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'category_id',
      referencedColumnName: 'id',
    },
  })
  @AutoMap(() => [Category])
  categories: Category[];

  public constructor(
    name: string,
    slug: string,
    shortDescription: string,
    description: string
  ) {
    this.name = name;
    this.slug = slug;
    this.shortDescription = shortDescription;
    this.description = description;
  }
}
