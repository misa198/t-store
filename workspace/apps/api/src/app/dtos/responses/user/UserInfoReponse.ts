import { AutoMap } from '@automapper/classes';
import { ImageResponse } from '../image/ImageResponse';
import { RoleResponse } from '../role/RoleResponse';

export class UserInfoResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  email: string;

  @AutoMap()
  firstName: string;

  @AutoMap()
  lastName: string;

  @AutoMap()
  role: RoleResponse;

  @AutoMap()
  profilePicture: ImageResponse;
}
