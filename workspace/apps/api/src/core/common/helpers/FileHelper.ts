import { BadRequestException } from '@nestjs/common';
import { Request } from 'express';
import { Constants } from '../constants/Contants';
import { MessageHelper } from './MessageHelper';

export class FileHandler {
  public static imageFileFilter(
    req: Request,
    file: Express.Multer.File,
    callback: (error: Error, acceptFile: boolean) => void
  ) {
    if (!file.originalname.match(Constants.REGEX.IMAGE)) {
      return callback(
        new BadRequestException(
          MessageHelper.getMessage(
            'messages.notAllowed',
            `'${file.originalname}'`
          )
        ),
        false
      );
    }
    return callback(null, true);
  }
}
