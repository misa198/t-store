import { createMap, Mapper } from '@automapper/core';
import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Injectable } from '@nestjs/common';
import { ProductResponse } from '../../../app/dtos/responses/product/ProductResponse';
import { VariantResponse } from '../../../app/dtos/responses/product/VariantResponse';
import { Product } from '../../entities/Product';
import { Variant } from '../../entities/Variant';
import { ProductInternalResponse } from '../../../app/dtos/responses/product/ProductInternalResponse';

@Injectable()
export class ProductProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, Product, ProductResponse);
      createMap(mapper, Product, ProductInternalResponse);
      createMap(mapper, Variant, VariantResponse);
    };
  }
}
