import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../components/common/Helmet';
import PageLayout from '../components/common/PageLayout';

const Page404: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('404.title')} />
      <PageLayout title={t('404.title')}>404</PageLayout>
    </>
  );
};

export default Page404;
