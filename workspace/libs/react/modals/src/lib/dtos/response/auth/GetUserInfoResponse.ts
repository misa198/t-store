import { Image, Role } from '../../../../index';

export interface GetUserInfoResponse {
  profilePicture?: Image;
  lastName: string;
  firstName: string;
  email: string;
  id: number;
  role: Role;
}
