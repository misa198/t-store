import { Button, useTheme } from '@mui/material';
import { SnackbarProvider } from 'notistack';
import { createRef, FC, PropsWithChildren } from 'react';
import { useTranslation } from 'react-i18next';

const ToastProvider: FC<PropsWithChildren<unknown>> = ({ children }) => {
  const notistackRef = createRef<SnackbarProvider>();
  const theme = useTheme();
  const { t } = useTranslation();

  const onClickDismiss = (key: string) => {
    notistackRef.current?.closeSnackbar(key);
  };

  return (
    <SnackbarProvider
      ref={notistackRef}
      maxSnack={1}
      action={(key) => (
        <Button
          sx={{
            color: theme.palette.common.white,
          }}
          onClick={() => onClickDismiss(key as string)}
        >
          {t('common.labels.dismiss')}
        </Button>
      )}
    >
      {children}
    </SnackbarProvider>
  );
};

export default ToastProvider;
