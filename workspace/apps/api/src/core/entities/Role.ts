import { AutoMap } from '@automapper/classes';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';

@Entity({
  name: 'role',
})
export class Role {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  @AutoMap()
  id: number;

  @Column({
    name: 'code',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  @AutoMap()
  code: string;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  @AutoMap()
  name: string;

  @OneToMany(() => User, (user) => user.role, {
    cascade: ['insert'],
  })
  users: User[];
}
