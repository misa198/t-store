export interface CategoryDetails {
  description: string;
  slug: string;
  name: string;
  id: number;
  isActive?: boolean;
}
