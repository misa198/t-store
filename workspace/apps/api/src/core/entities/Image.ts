import { AutoMap } from '@automapper/classes';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Banner } from './Banner';
import { Brand } from './Brand';
import { ProductImage } from './ProductImage';
import { User } from './User';

@Entity({
  name: 'image',
})
export class Image {
  @AutoMap()
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @AutoMap()
  @Column({
    name: 'url',
    type: 'varchar',
    length: 4000,
    nullable: false,
  })
  url: string;

  @AutoMap()
  @Column({
    name: 'code',
    type: 'varchar',
    length: 4000,
    nullable: false,
  })
  code: string;

  @AutoMap()
  @Column({
    name: 'is_valid',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  isValid: boolean;

  @AutoMap()
  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @AutoMap()
  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToOne(() => Banner, (banner) => banner.image, {
    cascade: ['insert'],
  })
  banner: Banner;

  @OneToOne(() => User, (user) => user.profilePicture, {
    cascade: ['insert'],
  })
  user: User;

  @OneToOne(() => Brand, (brand) => brand.image, {
    cascade: ['insert'],
  })
  brand: Brand;

  @OneToOne(() => ProductImage, (productImage) => productImage.image, {
    cascade: ['insert'],
  })
  productImage: ProductImage;

  public constructor(url: string, code: string) {
    this.url = url;
    this.code = code;
  }
}
