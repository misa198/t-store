import { createAsyncThunk } from '@reduxjs/toolkit';
import { JwtPayload, LoginRequest } from '@workspace/react/modals';
import {
  decodeToken,
  setAccessToken,
  setRefreshToken,
  validRole,
} from '../../../../utils/auth';
import { getUserInfoApi, loginApi } from '../../../api/authApi';
import { toast } from '../toast/toastSlice';

export const loginThunk = createAsyncThunk(
  'auth/loginThunk',
  async (reqBody: LoginRequest, { dispatch }) => {
    try {
      const res = await loginApi(reqBody);
      const payload = decodeToken(res.data.token) as JwtPayload;
      if (!validRole(payload.role)) {
        return dispatch(
          toast({
            variant: 'error',
            message: 'common.errors.unauthenticated',
          })
        );
      }
      setAccessToken(res.data.token);
      setRefreshToken(res.data.refreshToken);
      window.location.replace('/');
      return res;
    } catch (e) {
      dispatch(
        toast({
          variant: 'error',
          message: 'common.errors.unauthenticated',
        })
      );
      throw new Error();
    }
  }
);

export const getUserInfoThunk = createAsyncThunk('auth/getUserInfo', async () =>
  getUserInfoApi()
);
