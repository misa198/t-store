import { Constants } from '../core/common/constants/Contants';

export const environment = {
  [Constants.CONFIG_KEY.NODE_ENV]: 'development',
  [Constants.CONFIG_KEY.PORT]: parseInt(process.env.API_PORT, 10) || 8080,

  [Constants.CONFIG_KEY.JWT_SECRET_KEY]: process.env.JWT_SECRET_KEY || 's3cr3t',
  [Constants.CONFIG_KEY.JWT_EXPIRES_IN]: process.env.JWT_EXPIRES_IN || '10m',
  [Constants.CONFIG_KEY.JWT_RT_SECRET_KEY]:
    process.env.JWT_RT_SECRET_KEY || 's3cr3t_rt',
  [Constants.CONFIG_KEY.JWT_RT_EXPIRES_IN]:
    process.env.JWT_RT_EXPIRES_IN || '7d',
  [Constants.CONFIG_KEY.JWT_MAIL_CONFIRMATION_SECRET_KEY]:
    process.env.JWT_MAIL_CONFIRMATION_SECRET_KEY || 's3cr3t_mail',
  [Constants.CONFIG_KEY.JWT_MAIL_CONFIRMATION_EXPIRES_IN]:
    process.env.JWT_MAIL_CONFIRMATION_EXPIRES_IN || '15m',
  [Constants.CONFIG_KEY.JWT_MAIL_FORGOT_PASSWORD_SECRET_KEY]:
    process.env.JWT_MAIL_FORGOT_PASSWORD_SECRET_KEY || 's3cr3t_mail',
  [Constants.CONFIG_KEY.JWT_MAIL_FORGOT_PASSWORD_EXPIRES_IN]:
    process.env.JWT_MAIL_FORGOT_PASSWORD_EXPIRES_IN || '15m',

  [Constants.CONFIG_KEY.MAIL_USER]: process.env.MAIL_USER,
  [Constants.CONFIG_KEY.MAIL_PASSWORD]: process.env.MAIL_PASSWORD,
  [Constants.CONFIG_KEY.MAIL_BASE_URL]: process.env.MAIL_BASE_URL,

  [Constants.CONFIG_KEY.DB_HOST]: process.env.DB_HOST || 'localhost',
  [Constants.CONFIG_KEY.DB_PORT]: parseInt(process.env.DB_PORT, 10) || 5432,
  [Constants.CONFIG_KEY.DB_NAME]: process.env.DB_NAME || 't-store',
  [Constants.CONFIG_KEY.DB_USER]: process.env.DB_USER || 'admin',
  [Constants.CONFIG_KEY.DB_PASSWORD]: process.env.DB_PASSWORD || 'admin',

  [Constants.CONFIG_KEY.REDIS_HOST]: process.env.REDIS_HOST || 'localhost',
  [Constants.CONFIG_KEY.REDIS_PORT]:
    parseInt(process.env.REDIS_PORT, 10) || 6379,
  [Constants.CONFIG_KEY.REDIS_TTL]:
    parseInt(process.env.REDIS_TTL, 10) || 604800,

  [Constants.CONFIG_KEY.CLOUDINARY_CLOUD_NAME]:
    process.env.CLOUDINARY_CLOUD_NAME,
  [Constants.CONFIG_KEY.CLOUDINARY_API_KEY]: process.env.CLOUDINARY_API_KEY,
  [Constants.CONFIG_KEY.CLOUDINARY_API_SECRET]:
    process.env.CLOUDINARY_API_SECRET,
  [Constants.CONFIG_KEY.CLOUDINARY_FOLDER]: process.env.CLOUDINARY_FOLDER,
};
