import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Constants } from '../../core/common/constants/Contants';
import { MessageHelper } from '../../core/common/helpers/MessageHelper';

@Injectable()
export class MailService {
  constructor(
    private mailerService: MailerService,
    private config: ConfigService
  ) {}

  async sendUserConfirmation(email: string, name: string, token: string) {
    const url = `${this.config.get(
      Constants.CONFIG_KEY.MAIL_BASE_URL
    )}/auth/confirm-email?token=${token}`;

    await this.mailerService.sendMail({
      to: email,
      subject: MessageHelper.getMessage('mail.confirmation.subject'),
      template: 'confirmation.hbs',
      context: {
        name,
        url,
      },
    });
  }

  async sendForgotPasswordMail(email: string, token: string) {
    const url = `${this.config.get(
      Constants.CONFIG_KEY.MAIL_BASE_URL
    )}/auth/forgot-password?token=${token}`;

    await this.mailerService.sendMail({
      to: email,
      subject: MessageHelper.getMessage('mail.forgotPassword.subject'),
      template: 'forgot-password.hbs',
      context: {
        url,
      },
    });
  }
}
