import { VariantType } from 'notistack';

export interface Notification {
  key: string;
  message: string;
  variant: VariantType;
}
