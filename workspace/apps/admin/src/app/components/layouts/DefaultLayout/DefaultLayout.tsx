import { Box } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { FC } from 'react';
import { Outlet } from 'react-router-dom';
import { useWindowWidth } from '@react-hook/window-size';
import { useAppSelector } from '../../../app/hooks/Redux';
import {
  APP_DRAWER_CLOSE_WIDTH,
  APP_DRAWER_WIDTH,
} from '../../../constants/layout';
import AppBar from './AppBar';
import AppDrawer from './AppDrawer';

const DefaultLayout: FC = () => {
  const width = useWindowWidth();
  const theme = useTheme();
  const drawerOpen = useAppSelector((state) => state.layout.drawerOpen);
  const isMdUp = useMediaQuery(theme.breakpoints.up('md'));

  return (
    <Box>
      <AppBar />
      <Box sx={{ display: 'flex' }}>
        <Box
          sx={{
            transition: theme.transitions.create(['width'], {
              easing: theme.transitions.easing.sharp,
              duration: theme.transitions.duration.enteringScreen,
            }),
            ...(isMdUp && {
              width: APP_DRAWER_WIDTH,
            }),
            ...(isMdUp &&
              !drawerOpen && {
                width: `${APP_DRAWER_CLOSE_WIDTH + 1}px !important`,
              }),
          }}
        >
          <AppDrawer />
        </Box>
        <Box
          sx={{
            width: {
              md: `${
                width -
                (drawerOpen ? APP_DRAWER_WIDTH : APP_DRAWER_CLOSE_WIDTH) -
                1
              }px !important`,
              xs: `${width}px !important`,
            },
          }}
        >
          <Outlet />
        </Box>
      </Box>
    </Box>
  );
};

export default DefaultLayout;
