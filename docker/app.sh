#!/bin/bash

dev() {
  docker compose -f docker-compose.dev.yml run --rm --service-ports app
}

sh() {
  docker exec -it $@ /bin/ash
}

$@
