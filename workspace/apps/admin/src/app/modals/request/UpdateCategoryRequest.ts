export interface UpdateCategoryRequest {
  name: string;
  description: string;
  isActive: boolean;
}
