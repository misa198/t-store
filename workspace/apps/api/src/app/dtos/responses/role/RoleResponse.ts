import { AutoMap } from '@automapper/classes';

export class RoleResponse {
  @AutoMap()
  name: string;

  @AutoMap()
  code: string;
}
