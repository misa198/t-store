import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';
import {
  BRAND_DESCRIPTION_MAX_LENGTH,
  BRAND_NAME_MAX_LENGTH,
} from '@workspace/common/constants';

export class CreateBrandRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('brand.name'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(0, BRAND_NAME_MAX_LENGTH)
  name: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('brand.description'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(0, BRAND_DESCRIPTION_MAX_LENGTH)
  description: string;
}
