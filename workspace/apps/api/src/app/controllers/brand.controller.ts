import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { Constants } from '../../core/common/constants/Contants';
import { RoleEnum } from '../../core/common/enums/RoleEnum';
import { FileHandler } from '../../core/common/helpers/FileHelper';
import { JwtPayload } from '../../core/common/modals/JwtPayload';
import { ReqUser } from '../../core/security/req-user.decorator';
import { Roles } from '../../core/security/roles.decorator';
import { BrandService } from '../../core/services/brand.service';
import { CreateBrandRequest } from '../dtos/requests/brand/CreateBrandRequest';
import { GetBrandsQuery } from '../dtos/requests/brand/GetBrandsQuery';
import { UpdateBrandRequest } from '../dtos/requests/brand/UpdateBrandRequest';

@ApiTags('brands')
@Controller('brands')
export class BrandController {
  constructor(private readonly brandService: BrandService) {}

  @Post()
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async createBrand(@Body() createBrandRequest: CreateBrandRequest) {
    return this.brandService.insertBrand(createBrandRequest);
  }

  @Get()
  public async getAllBrands(
    @Query() query: GetBrandsQuery,
    @ReqUser() reqUser?: JwtPayload
  ) {
    return this.brandService.getAllBrands(query, reqUser);
  }

  @Get(':slug')
  public async getBrand(@Param('slug') slug, @ReqUser() reqUser?: JwtPayload) {
    return this.brandService.getBrand(slug, reqUser);
  }

  @Put(':slug')
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async updateBrand(
    @Param('slug') slug,
    @Body() updateBrandRequest: UpdateBrandRequest
  ) {
    return this.brandService.updateBrand(slug, updateBrandRequest);
  }

  @Delete(':slug')
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async deleteBrand(@Param('slug') slug) {
    return this.brandService.removeBrand(slug);
  }

  @Post(':slug/image')
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: FileHandler.imageFileFilter,
      limits: {
        fileSize: Constants.MAX_IMAGE_SIZE,
      },
    })
  )
  public async updateBrandImage(
    @Param('slug') slug,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.brandService.updateBrandImage(slug, file);
  }
}
