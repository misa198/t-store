import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';
import CategoryFilter from '../../components/pages/categories/CategoryFilter';
import CategoryTable from '../../components/pages/categories/CategoryTable';

const CategoriesIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('categories.title')} />
      <PageLayout title={t('categories.title')}>
        <CategoryFilter />
        <CategoryTable />
      </PageLayout>
    </>
  );
};

export default CategoriesIndex;
