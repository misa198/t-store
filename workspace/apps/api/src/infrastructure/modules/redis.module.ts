import { CacheModule, Global, Module } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';
import { Constants } from '../../core/common/constants/Contants';
import { environment } from '../../environments/environment';
import { RedisService } from '../services/redis.service';

@Global()
@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: environment[Constants.CONFIG_KEY.REDIS_HOST],
      port: environment[Constants.CONFIG_KEY.REDIS_PORT],
      ttl: environment[Constants.CONFIG_KEY.REDIS_TTL] as number,
      isGlobal: true,
    }),
  ],
  providers: [RedisService],
  exports: [RedisService],
})
export class RedisModule {}
