import { Mapper } from '@automapper/core';
import { InjectMapper } from '@automapper/nestjs';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import _ from 'lodash';
import { In } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateProductRequest } from '../../app/dtos/requests/product/CreateProductRequest';
import { GetProductsQuery } from '../../app/dtos/requests/product/GetProductsQuery';
import { BaseResponse } from '../../app/dtos/responses/common/BaseResponse';
import { PaginationMeta } from '../../app/dtos/responses/common/PaginationMeta';
import { ProductResponse } from '../../app/dtos/responses/product/ProductResponse';
import { ProductRepository } from '../../infrastructure/repositories/product.repository.s';
import { Constants } from '../common/constants/Contants';
import { ProductSortEnum } from '../common/enums/ProductSortEnum';
import { JwtPayload } from '../common/modals/JwtPayload';
import { Utils } from '../common/utils/Utils';
import { OrderItem } from '../entities/OrderItem';
import { Product } from '../entities/Product';
import { Variant } from '../entities/Variant';
import { BrandService } from './brand.service';
import { CategoryService } from './category.service';
import { ProductInternalResponse } from '../../app/dtos/responses/product/ProductInternalResponse';
import { UpdateProductRequest } from '../../app/dtos/requests/product/UpdateProductRequest';
import { VariantRepository } from '../../infrastructure/repositories/variant.repository';

@Injectable()
export class ProductService {
  constructor(
    private productRepository: ProductRepository,
    private variantRepository: VariantRepository,

    private readonly brandService: BrandService,
    private readonly categoryService: CategoryService,

    @InjectMapper()
    private readonly mapper: Mapper
  ) {}

  @Transactional()
  public async createProduct(
    requestBody: CreateProductRequest
  ): Promise<BaseResponse<ProductResponse>> {
    const validBrand = await this.brandService.isValidBrandId(
      requestBody.brandId
    );
    if (!validBrand.isValid)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });

    const validCategories = await this.categoryService.isValidCategoryIds(
      requestBody.categoryIds
    );
    if (!validCategories.isValid)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });

    const newProduct = new Product(
      requestBody.name,
      Utils.generateSlug(requestBody.name),
      requestBody.shortDescription,
      requestBody.description
    );
    newProduct.brand = validBrand.data;
    newProduct.categories = validCategories.data;
    const savedProduct = await this.productRepository.save(newProduct);
    const variants: Variant[] = [];
    requestBody.variants.forEach((variant) =>
      variants.push(
        new Variant(variant.name, variant.price, variant.quantity, savedProduct)
      )
    );
    await this.variantRepository.save(variants);
    return new BaseResponse(
      this.mapper.map(savedProduct, Product, ProductResponse)
    );
  }

  public async getAllProducts(
    query: GetProductsQuery,
    reqUser?: JwtPayload
  ): Promise<BaseResponse<ProductResponse[], PaginationMeta>> {
    if (
      !query.isActive &&
      !Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)
    ) {
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.FORBIDDEN,
      });
    }

    const builtQuery = this.productRepository
      .createQueryBuilder('product')
      .where(
        `product.is_active = ${query.isActive} AND product.is_valid = true`
      );

    switch (query.sort) {
      case ProductSortEnum.NAME_ASC: {
        builtQuery.orderBy({
          name: 'ASC',
        });
        break;
      }
      case ProductSortEnum.NAME_DESC: {
        builtQuery.orderBy({
          name: 'DESC',
        });
        break;
      }
      case ProductSortEnum.TIME_ASC: {
        builtQuery.orderBy({
          'product.created_at': 'ASC',
        });
        break;
      }
      case ProductSortEnum.TIME_DESC: {
        builtQuery.orderBy({
          'product.created_at': 'DESC',
        });
        break;
      }
      case ProductSortEnum.PRICE_ASC: {
        builtQuery
          .innerJoin(
            (qb) =>
              qb
                .from(Variant, 'variant')
                .select('MIN(price)', 'min_price')
                .addSelect('product_id', 'product_id')
                .groupBy('product_id'),
            'expensive_variant',
            'expensive_variant.product_id = product.id'
          )
          .orderBy('expensive_variant.min_price', 'ASC');
        break;
      }
      case ProductSortEnum.PRICE_DESC: {
        builtQuery
          .innerJoin(
            (qb) =>
              qb
                .from(Variant, 'variant')
                .select('MAX(price)', 'max_price')
                .addSelect('product_id', 'product_id')
                .groupBy('product_id'),
            'expensive_variant',
            'expensive_variant.product_id = product.id'
          )
          .orderBy('expensive_variant.max_price', 'DESC');
        break;
      }
      case ProductSortEnum.BEST_SELLER: {
        builtQuery
          .leftJoin(
            (qb) =>
              qb
                .from(OrderItem, 'order_item')
                .innerJoin(
                  Variant,
                  'variant',
                  'variant.id = order_item.variant_id'
                )
                .select('SUM(order_item.quantity)', 'quantity')
                .addSelect('variant.product_id', 'product_id')
                .groupBy('variant.product_id'),
            'total_sell',
            'total_sell.product_id = product.id'
          )
          .orderBy('total_sell.quantity', 'DESC');
      }
    }

    if (query.brand)
      builtQuery
        .innerJoin('product.brand', 'brand')
        .andWhere(`brand.slug = '${query.brand}'`);
    if (query.categories) {
      const categorySlugs = query.categories.split(',');
      builtQuery
        .innerJoin('product.categories', 'categories')
        .where({ 'categories.slug': In(categorySlugs) });
    }
    const [result, total] = await builtQuery
      .offset(query.offset)
      .limit(query.limit)
      .getManyAndCount();
    const productIds = result.map((product) => product.id);
    const products = await this.productRepository.find({
      where: { id: In(productIds) },
      relations: ['variants', 'brand', 'categories', 'productImages'],
    });
    const sortedProduct = _.sortBy(products, (item) => {
      return productIds.indexOf(item.id);
    });
    const meta = new PaginationMeta(total, query.offset, query.limit);
    return new BaseResponse(
      this.mapper.mapArray(sortedProduct, Product, ProductInternalResponse),
      meta
    );
  }

  public async getProduct(slug: string, reqUser?: JwtPayload) {
    const product = await this.findValidProductBySlug(slug, [
      'brand',
      'variants',
      'categories',
      'productImages',
      'productImages.image',
    ]);
    if (!product)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    if (
      !product.isActive &&
      !Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)
    )
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.FORBIDDEN,
      });
    if (Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)) {
      return new BaseResponse(
        this.mapper.map(product, Product, ProductInternalResponse)
      );
    }
    return new BaseResponse(this.mapper.map(product, Product, ProductResponse));
  }

  @Transactional()
  public async updateProduct(
    slug: string,
    updateProductBody: UpdateProductRequest
  ) {
    const product = await this.findValidProductBySlug(slug, ['variants']);
    if (!product)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    const validBrand = await this.brandService.isValidBrandId(
      updateProductBody.brandId
    );
    if (!validBrand.isValid)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });

    const validCategories = await this.categoryService.isValidCategoryIds(
      updateProductBody.categoryIds
    );
    if (!validCategories.isValid)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });

    product.name = updateProductBody.name;
    product.shortDescription = updateProductBody.shortDescription;
    product.description = updateProductBody.description;
    await this.productRepository.save(product);
    const variants: Variant[] = [];
    updateProductBody.variants.forEach((variant) =>
      variants.push(
        new Variant(variant.name, variant.price, variant.quantity, product)
      )
    );
    product.variants.forEach((v) => {
      if (v.isValid) {
        v.isValid = false;
        variants.push(v);
      }
    });
    await this.variantRepository.save(variants);
  }

  private async findValidProductBySlug(slug: string, relations: string[] = []) {
    const brand = await this.productRepository.findOne({
      where: { slug, isValid: true },
      relations,
    });
    if (!brand)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    return brand;
  }
}
