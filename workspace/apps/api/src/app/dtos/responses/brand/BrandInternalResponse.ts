import { AutoMap } from '@automapper/classes';
import { ImageResponse } from '../image/ImageResponse';

export class BrandInternalResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  name: string;

  @AutoMap()
  slug: string;

  @AutoMap()
  isActive: boolean;

  @AutoMap()
  image: ImageResponse;

  @AutoMap()
  description?: string;
}
