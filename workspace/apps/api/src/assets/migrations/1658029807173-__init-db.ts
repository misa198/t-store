import {MigrationInterface, QueryRunner} from "typeorm";

export class undefinedinitDb1658029807173 implements MigrationInterface {
    name = 'undefinedinitDb1658029807173'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "status" DROP CONSTRAINT "FK_08848d8d852672c1b3a02d9afda"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP CONSTRAINT "FK_9c09248ba902bb484de51916378"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "created_at"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP CONSTRAINT "UQ_08848d8d852672c1b3a02d9afda"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "order_id"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "payment_id"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "code"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "key"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "name"
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "key" character varying(200) NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "name" character varying(200) NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "code" character varying(4000) NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "order_id" bigint
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD CONSTRAINT "UQ_08848d8d852672c1b3a02d9afda" UNIQUE ("order_id")
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "payment_id" bigint
        `);
        await queryRunner.query(`
            ALTER TABLE "product" DROP COLUMN "average_rate"
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ADD "average_rate" double precision NOT NULL DEFAULT '0'
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD CONSTRAINT "FK_08848d8d852672c1b3a02d9afda" FOREIGN KEY ("order_id") REFERENCES "order"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD CONSTRAINT "FK_9c09248ba902bb484de51916378" FOREIGN KEY ("payment_id") REFERENCES "payment_method"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "status" DROP CONSTRAINT "FK_9c09248ba902bb484de51916378"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP CONSTRAINT "FK_08848d8d852672c1b3a02d9afda"
        `);
        await queryRunner.query(`
            ALTER TABLE "product" DROP COLUMN "average_rate"
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ADD "average_rate" integer NOT NULL DEFAULT '0'
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "payment_id"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP CONSTRAINT "UQ_08848d8d852672c1b3a02d9afda"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "order_id"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "created_at"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "code"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "name"
        `);
        await queryRunner.query(`
            ALTER TABLE "status" DROP COLUMN "key"
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "name" character varying(200) NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "key" character varying(200) NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "code" character varying(4000) NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "payment_id" bigint
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "order_id" bigint
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD CONSTRAINT "UQ_08848d8d852672c1b3a02d9afda" UNIQUE ("order_id")
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD CONSTRAINT "FK_9c09248ba902bb484de51916378" FOREIGN KEY ("payment_id") REFERENCES "payment_method"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "status"
            ADD CONSTRAINT "FK_08848d8d852672c1b3a02d9afda" FOREIGN KEY ("order_id") REFERENCES "order"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    }

}
