import { FC, useEffect, memo } from 'react';
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import { BrandDetails } from '@workspace/react/modals';
import * as yup from 'yup';
import {
  BRAND_DESCRIPTION_MAX_LENGTH,
  BRAND_NAME_MAX_LENGTH,
} from '@workspace/common/constants';
import { useTranslation } from 'react-i18next';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import { useFormik } from 'formik';
import { UpdateBrandRequest } from '../../../modals/request/UpdateBrandRequest';
import { updateBrandThunk } from '../../../app/store/features/brand/brandThunk';
import { useNavigate } from 'react-router-dom';

interface Props {
  brand: BrandDetails;
}

const EditBrandForm: FC<Props> = ({ brand }) => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const isEdited = useAppSelector((state) => state.brand.brand.isEdited);

  const initialValues: UpdateBrandRequest = {
    name: brand.name,
    description: brand.description,
    isActive: brand.isActive as boolean,
  };

  const validationSchema = yup.object().shape({
    name: yup
      .string()
      .max(
        BRAND_NAME_MAX_LENGTH,
        t('common.validator.max', { max: BRAND_NAME_MAX_LENGTH })
      )
      .required(t('common.validator.required')),
    description: yup
      .string()
      .max(
        BRAND_DESCRIPTION_MAX_LENGTH,
        t('common.validator.max', { max: BRAND_DESCRIPTION_MAX_LENGTH })
      ),
    isActive: yup.boolean().required(t('common.validator.required')),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      dispatch(
        updateBrandThunk({
          slug: brand.slug,
          brand: values,
        })
      );
    },
  });

  useEffect(() => {
    if (isEdited) {
      navigate(`/brands/${brand.slug}`);
    }
  }, [brand.slug]);

  return (
    <Box component="form">
      <Box>
        <TextField
          variant="outlined"
          fullWidth
          onChange={formik.handleChange}
          value={formik.values.name}
          helperText={formik.errors.name && formik.errors.name}
          error={Boolean(formik.errors.name)}
          name="name"
          label={t('brands.table.name')}
        />
      </Box>
      <Box sx={{ mt: 2 }}>
        <TextField
          multiline
          rows={5}
          fullWidth
          onChange={formik.handleChange}
          value={formik.values.description}
          helperText={formik.errors.description && formik.errors.description}
          error={Boolean(formik.errors.description)}
          name="description"
          label={t('brands.table.description')}
        />
        <Box>
          <Typography
            component="div"
            variant="caption"
            sx={{ textAlign: 'right', mr: 0.5 }}
          >
            {formik.values.description?.length || 0}/
            {BRAND_DESCRIPTION_MAX_LENGTH}
          </Typography>
        </Box>
      </Box>
      <Box sx={{ mt: 2 }}>
        <FormControl fullWidth>
          <InputLabel id="is-active">Active</InputLabel>
          <Select
            labelId="is-active"
            label="Active"
            fullWidth
            value={formik.values.isActive ? 'true' : 'false'}
            onChange={(e) => {
              formik.setFieldValue('isActive', e.target.value === 'true');
            }}
          >
            <MenuItem value="true">
              {t('brands.table.published.active')}
            </MenuItem>
            <MenuItem value="false">
              {t('brands.table.published.inactive')}
            </MenuItem>
          </Select>
        </FormControl>
      </Box>
      <Box sx={{ mt: 2, display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          onClick={() => formik.handleSubmit()}
          disabled={!formik.isValid}
        >
          {t('common.btn.save')}
        </Button>
      </Box>
    </Box>
  );
};

export default memo(EditBrandForm);
