import { FC } from 'react';
import { Box, Paper, IconButton, useTheme } from '@mui/material';
import backgroundImage from '../../../../assets/images/auth-background.png';
import { Navigate, Outlet } from 'react-router-dom';
import TranslateRoundedIcon from '@mui/icons-material/TranslateRounded';
import { useTranslation } from 'react-i18next';
import { getLanguage, toggleLanguage } from '../../../utils/language';
import { getAccessToken } from '../../../utils/auth';

const accessToken = getAccessToken();

const AuthLayout: FC = () => {
  const theme = useTheme();
  const { i18n } = useTranslation();

  const onToggleLanguage = () => {
    toggleLanguage();
    i18n.changeLanguage(getLanguage());
  };

  if (!accessToken)
    return (
      <Box
        sx={{
          width: '100vw',
          minHeight: '100vh',
          backgroundColor: theme.palette.secondary.main,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundImage: `url(${backgroundImage})`,
          backgroundSize: 'cover',
          position: 'relative',
        }}
      >
        <Paper
          elevation={4}
          sx={{
            width: {
              xs: '325px',
              sm: '500px',
            },
          }}
        >
          <Outlet />
        </Paper>
        <IconButton
          sx={{
            position: 'absolute',
            top: '10px',
            right: '10px',
            color: theme.palette.common.white,
          }}
          onClick={onToggleLanguage}
        >
          <TranslateRoundedIcon />
        </IconButton>
      </Box>
    );
  return <Navigate to="/" replace />;
};

export default AuthLayout;
