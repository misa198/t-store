import { createSlice } from '@reduxjs/toolkit';
import { Brand, BrandDetails } from '@workspace/react/modals';
import {
  createBrandThunk,
  getBrandsThunk,
  getBrandThunk,
  updateBrandThunk,
  deleteBrandThunk,
} from './brandThunk';
import { deleteCategoryThunk } from '../category/categoryThunk';

interface State {
  brands: {
    data: Brand[];
    loading: boolean;
  };
  brand: {
    data: BrandDetails | null;
    loading: boolean;
    error: boolean;
    isEdited: boolean;
  };
  newBrand: {
    data: string;
    loading: boolean;
    error: boolean;
  };
  deleteBrand: {
    loading: boolean;
    error: boolean;
  };
}

const initialState: State = {
  brands: {
    data: [],
    loading: false,
  },
  brand: {
    data: null,
    loading: false,
    error: false,
    isEdited: false,
  },
  newBrand: {
    data: '',
    loading: false,
    error: false,
  },
  deleteBrand: {
    loading: false,
    error: false,
  },
};

const brandSlice = createSlice({
  name: 'brand',
  initialState,
  reducers: {
    resetNewBrand: (state) => {
      state.newBrand.data = '';
      state.newBrand.loading = false;
      state.newBrand.error = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getBrandsThunk.pending, (state) => {
      state.brands.loading = true;
      state.brands.data = [];
    });
    builder.addCase(getBrandsThunk.fulfilled, (state, action) => {
      state.brands.loading = false;
      state.brands.data = action.payload;
    });

    builder.addCase(getBrandThunk.pending, (state) => {
      state.brand.data = null;
      state.brand.loading = true;
      state.brand.error = false;
      state.brand.isEdited = false;
    });
    builder.addCase(getBrandThunk.fulfilled, (state, action) => {
      state.brand.data = action.payload;
      state.brand.loading = false;
      state.brand.error = false;
    });
    builder.addCase(getBrandThunk.rejected, (state) => {
      state.brand.loading = false;
      state.brand.error = true;
    });

    builder.addCase(updateBrandThunk.pending, (state) => {
      state.brand.loading = true;
      state.brand.error = false;
    });
    builder.addCase(updateBrandThunk.fulfilled, (state, action) => {
      state.brand.data!.slug = action.payload.slug;
      state.brand.loading = false;
      state.brand.error = false;
      state.brand.isEdited = true;
    });
    builder.addCase(updateBrandThunk.rejected, (state) => {
      state.brand.loading = false;
      state.brand.error = true;
    });

    builder.addCase(createBrandThunk.pending, (state) => {
      state.newBrand.loading = true;
      state.newBrand.error = false;
    });
    builder.addCase(createBrandThunk.fulfilled, (state, action) => {
      state.newBrand.data = action.payload.slug;
      state.newBrand.loading = false;
      state.newBrand.error = false;
    });
    builder.addCase(createBrandThunk.rejected, (state) => {
      state.newBrand.loading = false;
      state.newBrand.error = true;
    });

    builder.addCase(deleteCategoryThunk.pending, (state) => {
      state.deleteBrand.loading = true;
      state.deleteBrand.error = false;
    });
    builder.addCase(deleteBrandThunk.fulfilled, (state, action) => {
      state.deleteBrand.loading = false;
      state.deleteBrand.error = false;
      state.brands.data = state.brands.data.filter(
        (brand) => brand.slug !== action.payload
      );
    });
    builder.addCase(deleteBrandThunk.rejected, (state) => {
      state.deleteBrand.loading = false;
      state.deleteBrand.error = true;
    });
  },
});

export default brandSlice.reducer;
export const { resetNewBrand } = brandSlice.actions;
