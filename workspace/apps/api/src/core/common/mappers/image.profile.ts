import { createMap, Mapper } from '@automapper/core';
import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Injectable } from '@nestjs/common';
import { ImageResponse } from '../../../app/dtos/responses/image/ImageResponse';
import { Image } from '../../entities/Image';

@Injectable()
export class ImageProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, Image, ImageResponse);
    };
  }
}
