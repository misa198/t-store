import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Category } from '../../core/entities/Category';

@EntityRepository(Category)
export class CategoryRepository extends BaseRepository<Category> {}
