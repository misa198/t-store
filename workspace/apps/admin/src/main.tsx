import { CommonTheme } from '@workspace/react/theme';
import i18next from 'i18next';
// import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';
import { HelmetProvider } from 'react-helmet-async';
import { initReactI18next } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './app/App';
import { store } from './app/app/store';
import ToastProvider from './app/components/common/Toast';
import { getLanguage } from './app/utils/language';
import en from './assets/i18n/en.json';
import vi from './assets/i18n/vi.json';

i18next.use(initReactI18next).init({
  resources: {
    en: {
      translation: en,
    },
    vi: {
      translation: vi,
    },
  },
  lng: getLanguage(),
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  // <StrictMode>
  <CommonTheme>
    <Provider store={store}>
      <HelmetProvider>
        <BrowserRouter>
          <ToastProvider>
            <App />
          </ToastProvider>
        </BrowserRouter>
      </HelmetProvider>
    </Provider>
  </CommonTheme>
  // </StrictMode>
);
