import { FC, PropsWithChildren, ReactNode, useState } from 'react';
import { Modal } from '@mui/material';

type Props = PropsWithChildren<unknown> & {
  Button: ReactNode;
};

const BasicModal: FC<Props> = ({ children, ...rest }) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      {Boolean(rest.Button) && <div onClick={handleOpen}>{rest.Button}</div>}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{
          minHeight: '100vh',
          width: '100vw',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <>{children}</>
      </Modal>
    </>
  );
};

export default BasicModal;
