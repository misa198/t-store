import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Notification } from '@workspace/react/modals';
import { randomString } from '../../../../utils/functions';
import { VariantType } from 'notistack';

interface State {
  notifications: Notification[];
}

const NOTIFICATION_ID_LENGTH = 10;

const initialState: State = {
  notifications: [],
};

const toastSlice = createSlice({
  name: 'toast',
  initialState,
  reducers: {
    toast: (
      state: State,
      action: PayloadAction<{
        message: string;
        variant: VariantType;
      }>
    ) => {
      state.notifications.push({
        ...action.payload,
        key: randomString(NOTIFICATION_ID_LENGTH),
      });
    },
    removeNotification: (state: State, action: PayloadAction<string>) => {
      state.notifications = state.notifications.filter(
        (notification) => notification.key !== action.payload
      );
    },
  },
});

export const { toast, removeNotification } = toastSlice.actions;
export default toastSlice.reducer;
