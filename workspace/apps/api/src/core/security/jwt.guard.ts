import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Constants } from '../common/constants/Contants';

@Injectable()
export class JwtGuard extends AuthGuard(Constants.JWT_STRATEGY_NAME) {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(_err, user) {
    return user;
  }
}
