import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './Order';
import { User } from './User';

@Entity({
  name: 'address',
})
export class Address {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'address',
    type: 'varchar',
    length: 500,
    nullable: false,
  })
  address: string;

  @Column({
    name: 'phone_number',
    type: 'varchar',
    length: 10,
    nullable: false,
  })
  phoneNumber: string;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  name: string;

  @Column({
    name: 'is_valid',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  isValid: boolean;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @ManyToOne(() => User, (user) => user.addresses, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'user_id',
  })
  user: User;

  @OneToMany(() => Order, (order) => order.address, {
    cascade: ['insert'],
  })
  orders: Order[];
}
