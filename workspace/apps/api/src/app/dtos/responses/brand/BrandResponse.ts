import { AutoMap } from '@automapper/classes';
import { ImageResponse } from '../image/ImageResponse';

export class BrandResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  name: string;

  @AutoMap()
  slug: string;

  @AutoMap()
  image: ImageResponse;

  @AutoMap()
  description?: string;
}
