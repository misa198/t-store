import { Box, CircularProgress, Container, Typography } from '@mui/material';
import { FC, PropsWithChildren } from 'react';

interface Props {
  title?: string;
  loading?: boolean;
}

const PageLayout: FC<PropsWithChildren<Props>> = ({
  title,
  children,
  loading = false,
}) => {
  return (
    <Container
      sx={{
        py: 4,
      }}
    >
      <Box sx={{ mb: 3 }}>
        <Typography
          variant="h5"
          sx={{
            textTransform: 'uppercase',
          }}
        >
          {title}
        </Typography>
      </Box>
      {loading && (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <CircularProgress />
        </Box>
      )}
      <Box>{children}</Box>
    </Container>
  );
};

export default PageLayout;
