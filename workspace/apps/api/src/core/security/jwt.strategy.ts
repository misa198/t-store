import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Constants } from '../common/constants/Contants';
import { JwtPayload } from '../common/modals/JwtPayload';

@Injectable()
export class JwtStrategy extends PassportStrategy(
  Strategy,
  Constants.JWT_STRATEGY_NAME
) {
  constructor(config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get(Constants.CONFIG_KEY.JWT_SECRET_KEY),
    });
  }

  async validate(payload: JwtPayload) {
    return {
      email: payload.email,
      id: payload.id,
      role: payload.role,
      firstName: payload.firstName,
      lastName: payload.lastName,
      profilePicture: payload.profilePicture,
    };
  }
}
