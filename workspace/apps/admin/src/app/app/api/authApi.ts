import {
  BaseResponse,
  GetUserInfoResponse,
  LoginRequest,
  LoginResponse,
} from '@workspace/react/modals';
import { api } from './axios';

const AUTH_URL = '/auth';
const AUTH_LOGIN_URL = `${AUTH_URL}/login`;
const AUTH_GET_USER_INFO_URL = `${AUTH_URL}/user`;

export const loginApi = async (
  reqBody: LoginRequest
): Promise<BaseResponse<LoginResponse>> => api.post(AUTH_LOGIN_URL, reqBody);

export const getUserInfoApi = async (): Promise<
  BaseResponse<GetUserInfoResponse>
> => api.get(AUTH_GET_USER_INFO_URL);
