import { AutoMap } from '@automapper/classes';
import { ImageResponse } from '../image/ImageResponse';

export class ProductImageResponse {
  @AutoMap()
  order: number;

  @AutoMap(() => ImageResponse)
  image: ImageResponse;
}
