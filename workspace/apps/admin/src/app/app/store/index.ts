import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { createBrowserHistory } from 'history';
import { createReduxHistoryContext } from 'redux-first-history';
import logger from 'redux-logger';
import authReducer from './features/auth/authSlice';
import layoutReducer from './features/layout/layoutSlice';
import toastReducer from './features/toast/toastSlice';
import brandReducer from './features/brand/brandSlice';
import categoryReducer from './features/category/categorySlice';

const { createReduxHistory, routerMiddleware, routerReducer } =
  createReduxHistoryContext({ history: createBrowserHistory() });

const rootReducer = combineReducers({
  layout: layoutReducer,
  auth: authReducer,
  router: routerReducer,
  toast: toastReducer,
  brand: brandReducer,
  category: categoryReducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(logger).concat(routerMiddleware),
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

export const history = createReduxHistory(store);
