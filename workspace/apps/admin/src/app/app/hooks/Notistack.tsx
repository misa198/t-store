import { useSnackbar } from 'notistack';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { removeNotification } from '../store/features/toast/toastSlice';
import { useAppDispatch, useAppSelector } from './Redux';

let displayedKeys: string[] = [];

export const useNotistack = () => {
  const dispatch = useAppDispatch();
  const notifications = useAppSelector((state) => state.toast.notifications);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { t } = useTranslation();

  useEffect(() => {
    notifications.forEach(({ message, variant, key }) => {
      if (displayedKeys.includes(key)) return;

      enqueueSnackbar(t(message), {
        key,
        variant,
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
        onExited: (e, notiKey) => {
          dispatch(removeNotification(notiKey as string));
          displayedKeys = [...displayedKeys.filter((k) => k !== notiKey)];
        },
      });

      displayedKeys = [...displayedKeys, key];
    });
  }, [notifications, closeSnackbar, enqueueSnackbar, dispatch, t]);
};
