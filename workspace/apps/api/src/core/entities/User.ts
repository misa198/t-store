import { AutoMap } from '@automapper/classes';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Address } from './Address';
import { Image } from './Image';
import { Order } from './Order';
import { Post } from './Post';
import { Role } from './Role';

@Entity({
  name: 'user',
})
export class User {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  @AutoMap()
  id: number;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  @AutoMap()
  email: string;

  @Column({
    name: 'password',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  password: string;

  @Column({
    name: 'first_name',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  @AutoMap()
  firstName: string;

  @Column({
    name: 'last_name',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  @AutoMap()
  lastName: string;

  @Column({
    name: 'banned',
    type: 'boolean',
    nullable: false,
    default: false,
  })
  banned: boolean;

  @Column({
    name: 'is_verified',
    type: 'boolean',
    nullable: false,
    default: false,
  })
  isVerified: boolean;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @ManyToOne(() => Role, (role) => role.users, {
    cascade: ['insert'],
  })
  @AutoMap(() => Role)
  @JoinColumn({
    name: 'role_id',
  })
  role: Role;

  @OneToMany(() => Address, (address) => address.user, {
    cascade: ['insert'],
  })
  addresses: Address[];

  @OneToMany(() => Order, (order) => order.user, {
    cascade: ['insert'],
  })
  orders: Order[];

  @OneToMany(() => Post, (post) => post.user, {
    cascade: ['insert'],
  })
  posts: Post[];

  @OneToOne(() => Image, (profilePicture) => profilePicture.user, {
    cascade: ['insert'],
    nullable: true,
  })
  @AutoMap()
  @JoinColumn({
    name: 'profile_picture_id',
  })
  profilePicture: Image;
}
