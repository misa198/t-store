import { JwtPayload as DefaultJwtpayload } from 'jsonwebtoken';

export interface JwtPayload extends DefaultJwtpayload {
  email: string;
  firstName: string;
  lastName: string;
  id: number;
  profilePicture?: string;
  role: string;
}
