import { FC } from 'react';
import LoginForm from '../../components/pages/auth/LoginForm';

const Login: FC = () => {
  return <LoginForm />;
};

export default Login;
