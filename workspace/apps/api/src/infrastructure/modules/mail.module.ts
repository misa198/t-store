import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Global, Module } from '@nestjs/common';
import { join } from 'path';
import { Constants } from '../../core/common/constants/Contants';
import { environment } from '../../environments/environment';
import { MailService } from '../services/mail.service';

@Global()
@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        service: 'gmail',
        auth: {
          user: environment[Constants.CONFIG_KEY.MAIL_USER] as string,
          pass: environment[Constants.CONFIG_KEY.MAIL_PASSWORD] as string,
        },
      },
      defaults: {
        from: `${Constants.MAIL_SYSTEM_NAME} <${
          environment[Constants.CONFIG_KEY.MAIL_USER]
        }>`,
      },
      template: {
        dir: join(__dirname, 'assets', 'mail-templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
