import _ from 'lodash';
import i18n from '../../../assets/i18n.json';

export class MessageHelper {
  public static getMessage(key: string, ...args): string {
    let rawMessage = _.get(i18n, key);
    if (args.length > 0 && rawMessage) {
      args.forEach((arg, i) => {
        rawMessage = rawMessage.replace(`{${i}}`, arg);
      });
    }
    return rawMessage;
  }
}
