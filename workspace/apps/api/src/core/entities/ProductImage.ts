import { AutoMap } from '@automapper/classes';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Image } from './Image';
import { Product } from './Product';

@Entity({
  name: 'product_image',
})
export class ProductImage {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  @AutoMap()
  id: number;

  @Column({
    name: 'order',
    type: 'int',
    nullable: false,
  })
  @AutoMap()
  order: number;

  @OneToOne(() => Image, (image) => image.productImage, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'image_id',
  })
  @AutoMap(() => Image)
  image: Image;

  @ManyToOne(() => Product, (product) => product.productImages, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'product_id',
  })
  product: Product;
}
