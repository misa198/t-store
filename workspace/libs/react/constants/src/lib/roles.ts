export enum ROLE_CODE {
  ADMIN = 'ADMIN',
  CONTENT = 'CONTENT',
  MANAGER = 'MANAGER',
}

export const ADMIN = {
  roleName: 'Admin',
  roleCode: ROLE_CODE.ADMIN,
};

export const CONTENT = {
  roleName: 'Content',
  roleCode: ROLE_CODE.CONTENT,
};

export const MANAGER = {
  roleName: 'Manager',
  roleCode: ROLE_CODE.MANAGER,
};

export const ROLES = {
  ADMIN,
  CONTENT,
  MANAGER,
};
