import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../components/common/Helmet';
import PageLayout from '../components/common/PageLayout';

const Page403: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('403.title')} />
      <PageLayout title={t('403.title')}>403</PageLayout>
    </>
  );
};

export default Page403;
