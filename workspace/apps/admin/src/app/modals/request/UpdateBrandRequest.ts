export interface UpdateBrandRequest {
  name: string;
  description: string;
  isActive: boolean;
}
