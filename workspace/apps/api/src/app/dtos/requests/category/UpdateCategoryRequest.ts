import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString, Length } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';
import { CATEGORY_NAME_MAX_LENGTH } from '@workspace/common/constants';

export class UpdateCategoryRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('category.name'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(0, CATEGORY_NAME_MAX_LENGTH)
  name: string;

  @ApiProperty({
    type: Boolean,
    required: true,
    description: MessageHelper.getMessage('category.isActive'),
  })
  @IsNotEmpty()
  @IsBoolean()
  isActive: boolean;
}
