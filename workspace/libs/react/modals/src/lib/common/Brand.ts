import { Image } from './Image';

export interface Brand {
  image?: Image;
  slug: string;
  name: string;
  id: number;
}
