import {
  BadRequestException,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { AuthGuard as PassportAuthGuard } from '@nestjs/passport';
import { Constants } from '../common/constants/Contants';
import { JwtService } from '../services/jwt.service';

@Injectable()
export class JwtRtGuard extends PassportAuthGuard(
  Constants.JWT_RT_STRATEGY_NAME
) {
  constructor(private jwtService: JwtService) {
    super();
  }

  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(_err, user, _info, context: ExecutionContext) {
    let refreshToken;
    try {
      refreshToken = context
        .switchToHttp()
        .getRequest()
        .headers.authorization?.replace('Bearer', '')
        .trim();
    } catch (e) {
      throw new BadRequestException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INVALID_CREDENTIALS,
      });
    }
    if (!refreshToken || user === false) {
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INVALID_CREDENTIALS,
      });
    }
    return { ...user, refreshToken };
  }
}
