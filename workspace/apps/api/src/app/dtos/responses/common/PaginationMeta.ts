export class PaginationMeta {
  private total: number;
  private offset: number;
  private limit: number;

  public constructor(total: number, offset: number, limit: number) {
    this.total = total;
    this.offset = offset;
    this.limit = limit;
  }
}
