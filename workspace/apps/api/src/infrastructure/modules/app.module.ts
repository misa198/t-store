import { Module } from '@nestjs/common';
import { AuthModule } from '../../core/modules/auth.module';
import { BrandModule } from '../../core/modules/brand.module';
import { CategoryModule } from '../../core/modules/category.module';
import { MapperModule } from '../../core/modules/mapper.module';
import { ProductModule } from '../../core/modules/product.module';
import { UserModule } from '../../core/modules/user.module';
import { CloudinaryModule } from './cloudinary.module';
import { ConfigModule } from './config.module';
import { DatabaseModule } from './database.module';
import { MailModule } from './mail.module';
import { RedisModule } from './redis.module';

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
    RedisModule,
    CloudinaryModule,
    MailModule,
    MapperModule,
    BrandModule,
    CategoryModule,
    ProductModule,
    UserModule,
    AuthModule,
  ],
})
export class AppModule {}
