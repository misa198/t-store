import { FC } from 'react';
import {
  Box,
  Button,
  InputAdornment,
  TextField,
  Typography,
} from '@mui/material';
import MailOutlineRoundedIcon from '@mui/icons-material/MailOutlineRounded';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { useFormik } from 'formik';
import { useTranslation } from 'react-i18next';
import * as yup from 'yup';
import { LoginRequest } from '@workspace/react/modals';
import {
  EMAIL_MAX_LENGTH,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
} from '@workspace/common/constants';
import { useAppDispatch } from '../../../app/hooks/Redux';
import { loginThunk } from '../../../app/store/features/auth/authThunks';

const initialValues: LoginRequest = {
  email: '',
  password: '',
};

const LoginForm: FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .max(
        EMAIL_MAX_LENGTH,
        t('common.validator.max', { max: EMAIL_MAX_LENGTH })
      )
      .email(t('common.validator.email'))
      .required(t('common.validator.required')),
    password: yup
      .string()
      .min(
        PASSWORD_MIN_LENGTH,
        t('common.validator.min', { min: PASSWORD_MIN_LENGTH })
      )
      .max(
        PASSWORD_MAX_LENGTH,
        t('common.validator.max', { max: PASSWORD_MAX_LENGTH })
      )
      .required(t('common.validator.required')),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values) => {
      dispatch(loginThunk(values));
    },
  });

  return (
    <Box
      component="form"
      sx={{
        width: '100%',
        py: 8,
        px: {
          xs: 2,
          sm: 8,
        },
      }}
      onSubmit={formik.handleSubmit}
    >
      <Typography
        variant="h4"
        sx={{
          textAlign: 'center',
          fontWeight: 600,
          mb: 2,
        }}
      >
        {t('auth.login')}
      </Typography>
      <Box
        sx={{
          mb: 2,
        }}
      >
        <TextField
          onChange={formik.handleChange}
          helperText={
            formik.errors.email && formik.touched.email && formik.errors.email
          }
          error={Boolean(formik.errors.email && formik.touched.email)}
          name="email"
          label={t('auth.email')}
          type="text"
          fullWidth
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <MailOutlineRoundedIcon />
              </InputAdornment>
            ),
          }}
        />
      </Box>
      <Box
        sx={{
          mb: 2,
        }}
      >
        <TextField
          helperText={
            formik.errors.password &&
            formik.touched.password &&
            formik.errors.password
          }
          error={Boolean(formik.errors.password && formik.touched.password)}
          onChange={formik.handleChange}
          name="password"
          label={t('auth.password')}
          type="password"
          fullWidth
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <LockOutlinedIcon />
              </InputAdornment>
            ),
          }}
        />
      </Box>
      <Box>
        <Button type="submit" variant="contained" size="large" fullWidth>
          {t('common.btn.submit')}
        </Button>
      </Box>
    </Box>
  );
};

export default LoginForm;
