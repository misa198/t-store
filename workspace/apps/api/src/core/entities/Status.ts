import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OrderStatus } from './OrderStatus';

@Entity({
  name: 'status',
})
export class Status {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'key',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  key: string;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  name: string;

  @OneToMany(() => OrderStatus, (orderStatus) => orderStatus.status, {
    cascade: ['insert'],
  })
  orderStatuses: OrderStatus[];
}
