import { Suspense, useEffect } from 'react';
import Router from './routes/Routes';
import '../assets/styles/global.css';
import { useNotistack } from './app/hooks/Notistack';
import { getAccessToken } from './utils/auth';
import { useAppDispatch } from './app/hooks/Redux';
import { getUserInfoThunk } from './app/store/features/auth/authThunks';

const App = () => {
  useNotistack();
  const dispatch = useAppDispatch();

  useEffect(() => {
    const token = getAccessToken();
    if (token) {
      dispatch(getUserInfoThunk());
    }
  }, [dispatch]);

  return (
    <Suspense fallback={<></>}>
      <Router />
    </Suspense>
  );
};

export default App;
