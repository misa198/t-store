import { JwtPayload as DefaultJwtpayload } from 'jsonwebtoken';

export interface JwtMailConfirmationPayload extends DefaultJwtpayload {
  email: string;
}
