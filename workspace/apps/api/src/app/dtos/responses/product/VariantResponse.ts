import { AutoMap } from '@automapper/classes';

export class VariantResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  name: string;

  @AutoMap()
  price: number;

  @AutoMap()
  quantity: number;
}
