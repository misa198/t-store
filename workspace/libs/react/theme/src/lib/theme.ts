import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: '#ff5384',
      contrastText: '#fff',
    },
    secondary: {
      main: '#FAFCFD',
    },
  },
  typography: {
    fontFamily: 'Nunito Sans, Helvetica, Arial, sans-serif',
    fontWeightMedium: 600,
    h5: {
      fontWeight: 600,
    },
    h6: {
      fontWeight: 600,
    },
    subtitle2: {
      fontWeight: 600,
    },
    button: {
      fontWeight: 600,
    },
  },
  shape: {
    borderRadius: 8,
  },
});

export default theme;
