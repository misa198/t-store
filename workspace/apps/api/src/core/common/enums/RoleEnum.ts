export enum RoleEnum {
  ADMIN = 'ADMIN',
  CONTENT = 'CONTENT',
  MANAGER = 'MANAGER',
  USER = 'USER',
}
