import { Mapper } from '@automapper/core';
import { InjectMapper } from '@automapper/nestjs';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { BaseResponse } from '../../app/dtos/responses/common/BaseResponse';
import { ImageResponse } from '../../app/dtos/responses/image/ImageResponse';
import { UserInfoResponse } from '../../app/dtos/responses/user/UserInfoReponse';
import { ImageRepository } from '../../infrastructure/repositories/image.repository';
import { UserRepository } from '../../infrastructure/repositories/user.repository';
import { CloudinaryService } from '../../infrastructure/services/cloudinary.service';
import { Constants } from '../common/constants/Contants';
import { JwtPayload } from '../common/modals/JwtPayload';
import { Utils } from '../common/utils/Utils';
import { Image } from '../entities/Image';
import { User } from '../entities/User';

@Injectable()
export class UserService {
  constructor(
    private userRepository: UserRepository,
    private imageRepository: ImageRepository,
    private readonly cloudinaryService: CloudinaryService,
    @InjectMapper()
    private readonly mapper: Mapper
  ) {}

  public async findByEmailWithRole(email: string) {
    return this.userRepository.findOne(
      { email },
      {
        relations: ['role'],
      }
    );
  }

  public async findByEmail(email: string) {
    return this.userRepository.findOne({
      where: { email },
    });
  }

  @Transactional()
  public async updateProfilePicture(id: number, file: Express.Multer.File) {
    if (!file)
      throw new BadRequestException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.BAD_REQUEST,
      });
    try {
      const user = await this.userRepository.findOne({
        where: { id },
        relations: ['profilePicture'],
      });
      const oldPicture = user.profilePicture;
      const res = await this.cloudinaryService.uploadImage(file);
      user.profilePicture = new Image(res.secure_url, res.public_id);
      let savedUser: User;
      if (oldPicture) {
        oldPicture.isValid = false;
        oldPicture.updatedAt = Utils.getDateTime();
        [, savedUser] = await Promise.all([
          this.imageRepository.save(oldPicture),
          this.userRepository.save(user),
        ]);
      } else {
        savedUser = await this.userRepository.save(user);
      }
      return new BaseResponse(
        this.mapper.map(savedUser.profilePicture, Image, ImageResponse)
      );
    } catch (e) {
      throw new InternalServerErrorException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INTERNAL_SERVER_ERROR,
      });
    }
  }

  @Transactional()
  public async deleteProfilePicture(id: number) {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: ['profilePicture'],
    });
    user.profilePicture.isValid = false;
    user.profilePicture.updatedAt = Utils.getDateTime();
    await this.userRepository.save(user);
  }

  public async getUserInfo(reqUser: JwtPayload) {
    const user = await this.userRepository.findOne({
      where: {
        id: reqUser.id,
      },
      relations: ['profilePicture', 'role'],
    });
    return new BaseResponse(this.mapper.map(user, User, UserInfoResponse));
  }
}
