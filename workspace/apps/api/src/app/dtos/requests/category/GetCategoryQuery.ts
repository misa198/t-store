import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';
import { CategorySortEnum } from '../../../../core/common/enums/CategorySortEnum';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class GetCategoriesQuery {
  @ApiProperty({
    type: Boolean,
    default: true,
    description: MessageHelper.getMessage('category.isActive'),
    required: false,
    enum: [true, false],
  })
  @Transform(({ value }) => value == 'true')
  @IsOptional()
  @IsBoolean()
  isActive = true;

  @ApiProperty({
    type: String,
    default: CategorySortEnum.NAME_ASC,
    description: MessageHelper.getMessage('common.sortBy'),
    required: false,
    enum: CategorySortEnum,
  })
  @IsOptional()
  @IsEnum(CategorySortEnum)
  sort = CategorySortEnum.NAME_ASC;
}
