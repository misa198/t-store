import { Box, IconButton } from '@mui/material';
import ModeEditOutlinedIcon from '@mui/icons-material/ModeEditOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { PAGINATION_ROWS_PER_PAGE_OPTIONS } from '../../../constants/common';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import {
  getBrandsThunk,
  deleteBrandThunk,
} from '../../../app/store/features/brand/brandThunk';
import {
  Brand,
  GetBrandsIsActive,
  GetBrandsQuery,
  GetBrandsSort,
} from '@workspace/react/modals';
import { Link, useSearchParams } from 'react-router-dom';
import {
  BRAND_ACTIVE_KEY,
  BRAND_SEARCH_KEY,
  BRAND_SORT_KEY,
} from '../../../constants/keys';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { DEFAULT_BRAND_IMAGE } from '@workspace/react/constants';
import Loading from '../../common/Loading';

const BrandTable: FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const [searchParams] = useSearchParams();
  const brands = useAppSelector((state) => state.brand.brands);
  const [pageSize, setPageSize] = useState(PAGINATION_ROWS_PER_PAGE_OPTIONS[0]);
  const [displayedBrands, setDisplayedBrands] = useState<Brand[]>([]);
  const active = searchParams.get(BRAND_ACTIVE_KEY);
  const sort = searchParams.get(BRAND_SORT_KEY);
  const keyword = searchParams.get(BRAND_SEARCH_KEY);

  const columns: GridColDef[] = [
    {
      field: 'id',
      headerName: t('brands.table.id'),
      sortable: false,
      disableColumnMenu: true,
      width: 50,
      align: 'center',
      editable: false,
      headerAlign: 'center',
      valueGetter: (params) => params.api.getRowIndex(params.row.id) + 1,
    },
    {
      field: 'image',
      headerName: t('brands.table.image'),
      sortable: false,
      editable: false,
      disableColumnMenu: true,
      width: 100,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params) => {
        return (
          <Box
            sx={{
              height: '35px',
              width: '35px',
              borderRadius: '50%',
              backgroundSize: 'cover',
              backgroundImage: `url(${
                params.row.image ? params.row.image.url : DEFAULT_BRAND_IMAGE
              })`,
            }}
          />
        );
      },
    },
    {
      field: 'name',
      headerName: t('brands.table.name'),
      sortable: false,
      editable: false,
      disableColumnMenu: true,
      flex: 1,
      minWidth: 300,
    },
    {
      field: 'actions',
      headerName: t('brands.table.actions'),
      sortable: false,
      editable: false,
      disableColumnMenu: true,
      align: 'center',
      headerAlign: 'center',
      width: 120,
      renderCell: (params) => (
        <Box>
          <Link to={`/brands/${params.row.slug}`}>
            <IconButton size="small">
              <ModeEditOutlinedIcon fontSize="small" />
            </IconButton>
          </Link>
          <IconButton
            size="small"
            onClick={() => onDeleteBrand(params.row.slug)}
          >
            <DeleteOutlineOutlinedIcon fontSize="small" />
          </IconButton>
        </Box>
      ),
    },
  ];

  useEffect(() => {
    const query: GetBrandsQuery = {};
    if (active) query.isActive = active as GetBrandsIsActive;
    if (sort) query.sort = sort as GetBrandsSort;
    dispatch(getBrandsThunk(query));
  }, [dispatch, active, sort]);

  useEffect(() => {
    setDisplayedBrands(
      keyword
        ? brands.data.filter((b) =>
            b.name.toLowerCase().includes(keyword.toLowerCase())
          )
        : brands.data
    );
  }, [brands.data, keyword]);

  const onPageSizeChange = (newValue: number) => {
    setPageSize(newValue);
  };

  const onDeleteBrand = (slug: string) => {
    dispatch(deleteBrandThunk(slug));
  };

  return (
    <Box sx={{ mt: 4, overflowX: 'auto', maxHeight: '500px' }}>
      <DataGrid
        autoHeight
        columns={columns}
        rows={displayedBrands}
        rowsPerPageOptions={PAGINATION_ROWS_PER_PAGE_OPTIONS}
        disableColumnSelector
        disableSelectionOnClick
        pagination
        pageSize={pageSize}
        onPageSizeChange={onPageSizeChange}
        localeText={{
          MuiTablePagination: {
            labelDisplayedRows: ({ from, to, count }) =>
              t('common.labels.displayedRows', {
                from,
                to,
                count,
              }),
            labelRowsPerPage: t('common.labels.rowsPerPage'),
          },
        }}
        components={{
          LoadingOverlay: Loading,
        }}
        loading={brands.loading}
      />
    </Box>
  );
};

export default BrandTable;
