import { createMap, Mapper } from '@automapper/core';
import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Injectable } from '@nestjs/common';
import { CategoryInternalResponse } from '../../../app/dtos/responses/category/CategoryInternalResponse';
import { CategoryResponse } from '../../../app/dtos/responses/category/CategoryResponse';
import { Category } from '../../entities/Category';

@Injectable()
export class CategoryProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, Category, CategoryResponse);
      createMap(mapper, Category, CategoryInternalResponse);
    };
  }
}
