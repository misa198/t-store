export type GetBrandsSort = 'name_asc' | 'name_desc' | 'time_asc' | 'time_desc';
export type GetBrandsIsActive = "false" | 'true'

export interface GetBrandsQuery {
  isActive?: GetBrandsIsActive;
  sort?: GetBrandsSort;
}
