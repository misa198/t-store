import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import toStream from 'buffer-to-stream';
import { UploadApiErrorResponse, UploadApiResponse, v2 } from 'cloudinary';
import { Constants } from '../../core/common/constants/Contants';

@Injectable()
export class CloudinaryService {
  constructor(private config: ConfigService) {}

  public async uploadImage(
    file: Express.Multer.File
  ): Promise<UploadApiResponse | UploadApiErrorResponse> {
    return new Promise((resolve, reject) => {
      const upload = v2.uploader.upload_stream(
        {
          folder: this.config.get(Constants.CONFIG_KEY.CLOUDINARY_FOLDER),
        },
        (error, result) => {
          if (error) return reject(error);
          resolve(result);
        }
      );
      toStream(file.buffer).pipe(upload);
    });
  }

  public async deleteImage(id: string) {
    return new Promise<UploadApiResponse | UploadApiErrorResponse>(
      (resolve, reject) => {
        v2.uploader.destroy(id, (error, result) => {
          if (error) reject(error);
          resolve(result);
        });
      }
    );
  }
}
