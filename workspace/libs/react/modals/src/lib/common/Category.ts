export interface Category {
  slug: string;
  name: string;
  id: number;
}
