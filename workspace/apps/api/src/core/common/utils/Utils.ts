import { nanoid } from 'nanoid';
import slugify from 'slugify';
import { Constants } from '../constants/Contants';

export class Utils {
  public static generateSlug(input: string) {
    return slugify(`${input} ${this.genUnique(Constants.SLUG_ID_LENGTH)}`, {
      lower: true,
      trim: true,
      strict: true,
    });
  }

  public static getDateTime(): Date {
    return new Date();
  }

  public static genUnique(length: number): string {
    return nanoid(length);
  }
}
