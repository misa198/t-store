import { FC, lazy, useEffect } from 'react';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';
import { useTranslation } from 'react-i18next';
import { useAppDispatch, useAppSelector } from '../../app/hooks/Redux';
import { getBrandThunk } from '../../app/store/features/brand/brandThunk';
import { useParams } from 'react-router-dom';
import EditBrandForm from '../../components/pages/brands/EditBrandForm';

const Error = lazy(() => import('../../components/common/Error'));

const Edit: FC = () => {
  const { slug } = useParams();
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const brand = useAppSelector((state) => state.brand.brand);

  useEffect(() => {
    dispatch(getBrandThunk(slug as string));
  }, [dispatch, slug]);

  if (brand.loading)
    return (
      <>
        <Helmet title={t('brandEdit.title')} />
        <PageLayout loading />
      </>
    );
  if (brand.data) {
    return (
      <>
        <Helmet title={brand.data.name} />
        <PageLayout title={brand.data.name}>
          <EditBrandForm brand={brand.data} />
        </PageLayout>
      </>
    );
  }
  if (brand.error)
    return (
      <>
        <Helmet title={t('common.errors.index')} />
        <PageLayout>
          <Error />
        </PageLayout>
      </>
    );
  return <></>;
};

export default Edit;
