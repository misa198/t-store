import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsNoneRoundedIcon from '@mui/icons-material/NotificationsNoneRounded';
import TranslateRoundedIcon from '@mui/icons-material/TranslateRounded';
import { Menu, MenuItem, useTheme } from '@mui/material';
import MuiAppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import { DEFAULT_PROFILE_PICTURE } from '@workspace/common/constants';
import { FC, MouseEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import logo from '../../../../assets/images/logo.png';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import { logout } from '../../../app/store/features/auth/authSlice';
import { toggleDrawer } from '../../../app/store/features/layout/layoutSlice';
import { getLanguage, toggleLanguage } from '../../../utils/language';
import Image from '../../common/Image';

const AppBar: FC = () => {
  const theme = useTheme();
  const { t, i18n } = useTranslation();
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const userMenuOpen = Boolean(anchorEl);
  const isMdUp = useMediaQuery(theme.breakpoints.up('md'));

  const onToggleDrawer = () => {
    dispatch(toggleDrawer());
  };

  const onOpenUserMenu = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onCloseUserMenu = () => {
    setAnchorEl(null);
  };

  const onLogout = () => {
    dispatch(logout());
  };

  const onToggleLanguage = () => {
    toggleLanguage();
    i18n.changeLanguage(getLanguage());
  };

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <MuiAppBar
          position="fixed"
          sx={{
            backgroundColor: theme.palette.common.black,
            zIndex: isMdUp ? theme.zIndex.drawer + 1 : theme.zIndex.drawer,
          }}
        >
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              pl: '16px !important',
            }}
          >
            <Box sx={{ flexGrow: 1, display: 'flex', alignItems: 'center' }}>
              <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 2 }}
                onClick={onToggleDrawer}
              >
                <MenuIcon />
              </IconButton>
              <Image
                sx={{
                  height: '20px',
                }}
                src={logo}
                alt="logo"
              />
            </Box>
            {!user.loading && user.data && (
              <>
                <IconButton color="inherit" sx={{ mr: 1.5 }}>
                  <NotificationsNoneRoundedIcon fontSize="medium" />
                </IconButton>
                <IconButton
                  id="user-menu-button"
                  color="inherit"
                  onClick={onOpenUserMenu}
                  aria-controls={userMenuOpen ? 'user-menu' : undefined}
                  aria-expanded={userMenuOpen ? 'true' : undefined}
                  aria-haspopup="true"
                >
                  <Box
                    sx={{
                      width: 35,
                      height: 35,
                      borderRadius: 20,
                      backgroundImage: `url(${DEFAULT_PROFILE_PICTURE})`,
                      backgroundSize: 'cover',
                    }}
                  />
                </IconButton>
                <Menu
                  id="user-menu"
                  anchorEl={anchorEl}
                  open={userMenuOpen}
                  onClose={onOpenUserMenu}
                  onClick={onCloseUserMenu}
                  MenuListProps={{
                    'aria-labelledby': 'user-menu-button',
                  }}
                  transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                  anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                >
                  <MenuItem>
                    <Box
                      sx={{
                        mr: 1,
                        ml: -0.5,
                        width: 32,
                        height: 32,
                        borderRadius: 20,
                        backgroundImage: `url(${DEFAULT_PROFILE_PICTURE})`,
                        backgroundSize: 'cover',
                      }}
                    />
                    <Box>
                      <Typography variant="subtitle1">{`${user.data.firstName} ${user.data.lastName}`}</Typography>
                      <Typography variant="body2">
                        {user.data.role.name}
                      </Typography>
                    </Box>
                  </MenuItem>
                  <Divider />
                  <MenuItem onClick={onToggleLanguage}>
                    <ListItemIcon>
                      <TranslateRoundedIcon fontSize="small" />
                    </ListItemIcon>
                    {t('common.appbar.language')}
                  </MenuItem>
                  <MenuItem onClick={onLogout}>
                    <ListItemIcon>
                      <ExitToAppIcon fontSize="small" />
                    </ListItemIcon>
                    {t('common.appbar.logout')}
                  </MenuItem>
                </Menu>
              </>
            )}
          </Toolbar>
        </MuiAppBar>
      </Box>
      <Toolbar />
    </>
  );
};

export default AppBar;
