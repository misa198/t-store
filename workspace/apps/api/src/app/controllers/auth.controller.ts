import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { JwtPayload } from '../../core/common/modals/JwtPayload';
import { JwtRtReqPayload } from '../../core/common/modals/JwtRtReqPayload';
import { AuthGuard } from '../../core/security/auth.guard';
import { JwtRtGuard } from '../../core/security/jwt-rt.guard';
import { ReqUser } from '../../core/security/req-user.decorator';
import { AuthService } from '../../core/services/auth.service';
import { UserService } from '../../core/services/user.service';
import { ConfirmationEmailQuery } from '../dtos/requests/auth/ConfirmationEmailQuery';
import { ForgotPasswordRequest } from '../dtos/requests/auth/ForgotPasswordRequest';
import { LoginRequest } from '../dtos/requests/auth/LoginRequest';
import { RegisterRequest } from '../dtos/requests/auth/RegisterRequest';
import { ResetPasswordRequest } from '../dtos/requests/auth/ResetPasswordRequest';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService
  ) {}

  @Post('/login')
  public async login(@Body() loginRequest: LoginRequest) {
    return this.authService.login(loginRequest);
  }

  @Post('/register')
  public async register(@Body() registerRequest: RegisterRequest) {
    return this.authService.register(registerRequest);
  }

  @UseGuards(JwtRtGuard)
  @Post('/refresh')
  public async refreshToken(@ReqUser() reqUser: JwtRtReqPayload) {
    return this.authService.refreshToken(reqUser);
  }

  @UseGuards(AuthGuard)
  @Post('/resend-confirmation-email')
  public async resendConfirmationEmail(@ReqUser() reqUser: JwtPayload) {
    return this.authService.resendConfirmationEmail(reqUser);
  }

  @Get('/confirm-email')
  public async confirmEmail(@Query() query: ConfirmationEmailQuery) {
    return this.authService.confirmEmail(query.token);
  }

  @Post('/forgot-password')
  public async forgotPassword(
    @Body() forgotPasswordRequest: ForgotPasswordRequest
  ) {
    return this.authService.forgotPassword(forgotPasswordRequest.email);
  }

  @Post('/reset-password')
  public async resetPassword(
    @Body() resetPasswordRequest: ResetPasswordRequest
  ) {
    return this.authService.resetPassword(resetPasswordRequest);
  }

  @UseGuards(AuthGuard)
  @Get('/user')
  public async getUserInfo(@ReqUser() reqUser: JwtPayload) {
    return this.userService.getUserInfo(reqUser);
  }
}
