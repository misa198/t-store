import { createSlice } from '@reduxjs/toolkit';
import { GetUserInfoResponse } from '@workspace/react/modals';
import { getUserInfoThunk, loginThunk } from './authThunks';
import { removeAllAuthCookies } from '../../../../utils/auth';

interface State {
  user: {
    data: GetUserInfoResponse | null;
    loading: boolean;
    error: boolean;
  };
  login: {
    loading: boolean;
    error: boolean;
  };
}

const initialState: State = {
  user: {
    data: null,
    loading: false,
    error: false,
  },
  login: {
    loading: false,
    error: false,
  },
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: () => {
      removeAllAuthCookies();
      window.location.href = '/auth/login';
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loginThunk.pending, (state) => {
      state.login.loading = true;
      state.login.error = false;
    });
    builder.addCase(loginThunk.fulfilled, (state) => {
      state.login.loading = false;
    });
    builder.addCase(loginThunk.rejected, (state) => {
      state.login.loading = false;
      state.login.error = true;
    });

    builder.addCase(getUserInfoThunk.pending, (state) => {
      state.user.loading = true;
    });
    builder.addCase(getUserInfoThunk.fulfilled, (state, action) => {
      state.user.data = action.payload.data;
      state.user.loading = false;
    });
    builder.addCase(getUserInfoThunk.rejected, (state) => {
      state.user.data = null;
      state.user.loading = false;
      state.user.error = true;
    });
  },
});

export const { logout } = authSlice.actions;
export default authSlice.reducer;
