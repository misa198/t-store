import { Global, Module, Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { v2 } from 'cloudinary';
import { Constants } from '../../core/common/constants/Contants';
import { CloudinaryService } from '../services/cloudinary.service';

const provider: Provider = {
  provide: 'CLOUDINARY',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    v2.config({
      cloud_name: config.get(Constants.CONFIG_KEY.CLOUDINARY_CLOUD_NAME),
      api_key: config.get(Constants.CONFIG_KEY.CLOUDINARY_API_KEY),
      api_secret: config.get(Constants.CONFIG_KEY.CLOUDINARY_API_SECRET),
    }),
};

@Global()
@Module({
  providers: [provider, CloudinaryService],
  exports: [provider, CloudinaryService],
})
export class CloudinaryModule {}
