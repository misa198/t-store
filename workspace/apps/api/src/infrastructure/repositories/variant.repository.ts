import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Variant } from '../../core/entities/Variant';

@EntityRepository(Variant)
export class VariantRepository extends BaseRepository<Variant> {}
