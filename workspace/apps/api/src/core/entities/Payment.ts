import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './Order';
import { PaymentMethod } from './PaymentMethod';

@Entity({
  name: 'status',
})
export class Payment {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'code',
    type: 'varchar',
    length: 4000,
    nullable: false,
  })
  code: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @OneToOne(() => Order, (order) => order.payment, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'order_id',
  })
  order: Order;

  @ManyToOne(() => PaymentMethod, (paymentMethod) => paymentMethod.payments, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'payment_id',
  })
  paymentMethod: PaymentMethod;
}
