import { Mapper } from '@automapper/core';
import { InjectMapper } from '@automapper/nestjs';
import {
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { FindManyOptions } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateBrandRequest } from '../../app/dtos/requests/brand/CreateBrandRequest';
import { GetBrandsQuery } from '../../app/dtos/requests/brand/GetBrandsQuery';
import { UpdateBrandRequest } from '../../app/dtos/requests/brand/UpdateBrandRequest';
import { BrandInternalResponse } from '../../app/dtos/responses/brand/BrandInternalResponse';
import { BrandResponse } from '../../app/dtos/responses/brand/BrandResponse';
import { BaseResponse } from '../../app/dtos/responses/common/BaseResponse';
import { ImageResponse } from '../../app/dtos/responses/image/ImageResponse';
import { BrandRepository } from '../../infrastructure/repositories/brand.repository';
import { ImageRepository } from '../../infrastructure/repositories/image.repository';
import { CloudinaryService } from '../../infrastructure/services/cloudinary.service';
import { Constants } from '../common/constants/Contants';
import { BrandSortEnum } from '../common/enums/BrandSortEnum';
import { JwtPayload } from '../common/modals/JwtPayload';
import { Utils } from '../common/utils/Utils';
import { Brand } from '../entities/Brand';
import { Image } from '../entities/Image';

@Injectable()
export class BrandService {
  constructor(
    private brandRepository: BrandRepository,
    private imageRepository: ImageRepository,
    private readonly cloudinaryService: CloudinaryService,
    @InjectMapper()
    private readonly mapper: Mapper
  ) {}

  @Transactional()
  public async insertBrand(createBrandRequest: CreateBrandRequest) {
    const newBrand = await this.brandRepository.save({
      name: createBrandRequest.name,
      slug: Utils.generateSlug(createBrandRequest.name),
      description: createBrandRequest.description,
    });
    const newBrandResponse = this.mapper.map(
      newBrand,
      Brand,
      BrandInternalResponse
    );
    return new BaseResponse(newBrandResponse);
  }

  public async getAllBrands(
    query: GetBrandsQuery,
    reqUser?: JwtPayload
  ): Promise<BaseResponse<BrandResponse[]>> {
    if (
      !query.isActive &&
      !Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)
    ) {
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.FORBIDDEN,
      });
    }

    const queryCondition: FindManyOptions<Brand> = {
      relations: ['image'],
    };
    queryCondition.where = {
      isActive: query.isActive,
      isValid: true,
    };
    switch (query.sort) {
      case BrandSortEnum.NAME_ASC: {
        queryCondition.order = {
          name: 'ASC',
        };
        break;
      }
      case BrandSortEnum.NAME_DESC: {
        queryCondition.order = {
          name: 'DESC',
        };
        break;
      }
      case BrandSortEnum.TIME_ASC: {
        queryCondition.order = {
          createdAt: 'ASC',
        };
        break;
      }
      case BrandSortEnum.TIME_DESC: {
        queryCondition.order = {
          createdAt: 'DESC',
        };
        break;
      }
    }
    const brands = await this.brandRepository.find(queryCondition);
    if (Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)) {
      return new BaseResponse(
        this.mapper.mapArray(brands, Brand, BrandInternalResponse)
      );
    }
    return new BaseResponse(this.mapper.mapArray(brands, Brand, BrandResponse));
  }

  public async getBrand(
    slug: string,
    reqUser?: JwtPayload
  ): Promise<BaseResponse<BrandResponse>> {
    const brand = await this.findValidBrandBySlug(slug, ['image']);
    if (!brand)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    if (
      !Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role) &&
      !brand.isActive
    )
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.FORBIDDEN,
      });
    if (Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)) {
      return new BaseResponse(
        this.mapper.map(brand, Brand, BrandInternalResponse)
      );
    }
    return new BaseResponse(this.mapper.map(brand, Brand, BrandResponse));
  }

  @Transactional()
  public async updateBrand(
    slug: string,
    brandRequest: UpdateBrandRequest
  ): Promise<BaseResponse<BrandResponse>> {
    const brand = await this.findValidBrandBySlug(slug);
    if (!brand)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    brand.name = brandRequest.name;
    brand.slug = Utils.generateSlug(brandRequest.name);
    brand.updatedAt = Utils.getDateTime();
    brand.isActive = brandRequest.isActive;
    await this.brandRepository.save(brand);
    return new BaseResponse(
      this.mapper.map(brand, Brand, BrandInternalResponse)
    );
  }

  @Transactional()
  public async removeBrand(slug: string): Promise<void> {
    const brand = await this.findValidBrandBySlug(slug);
    brand.isValid = false;
    brand.updatedAt = Utils.getDateTime();
    await this.brandRepository.save(brand);
  }

  @Transactional()
  public async updateBrandImage(slug: string, file: Express.Multer.File) {
    const brand = await this.findValidBrandBySlug(slug, ['image']);
    const oldImage = brand.image;
    try {
      const res = await this.cloudinaryService.uploadImage(file);
      brand.image = new Image(res.secure_url, res.public_id);
      if (oldImage) {
        oldImage.isValid = false;
        await Promise.all([
          this.imageRepository.save(oldImage),
          this.brandRepository.save(brand),
        ]);
      } else {
        await this.brandRepository.save(brand);
      }
      return new BaseResponse<ImageResponse>(
        this.mapper.map(brand.image, Image, ImageResponse)
      );
    } catch (e) {
      throw new InternalServerErrorException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INTERNAL_SERVER_ERROR,
      });
    }
  }

  public async isValidBrandId(id: number) {
    const brand = await this.brandRepository.findOne({
      where: { id, isValid: true },
    });
    if (brand)
      return {
        isValid: true,
        data: brand,
      };
    return {
      isValid: false,
    };
  }

  private async findValidBrandBySlug(slug: string, relations: string[] = []) {
    const brand = await this.brandRepository.findOne({
      where: { slug, isValid: true },
      relations: relations,
    });
    if (!brand)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    return brand;
  }
}
