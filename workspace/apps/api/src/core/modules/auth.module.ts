import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from '../../app/controllers/auth.controller';
import { RedisModule } from '../../infrastructure/modules/redis.module';
import { ImageRepository } from '../../infrastructure/repositories/image.repository';
import { RoleRepository } from '../../infrastructure/repositories/role.repository';
import { UserRepository } from '../../infrastructure/repositories/user.repository';
import { RedisService } from '../../infrastructure/services/redis.service';
import { JwtRtStrategy } from '../security/jwt-rt.strategy';
import { JwtGuard } from '../security/jwt.guard';
import { JwtStrategy } from '../security/jwt.strategy';
import { AuthService } from '../services/auth.service';
import { JwtService } from '../services/jwt.service';
import { PasswordService } from '../services/password.service';
import { RefreshTokenService } from '../services/refresh-token.service';
import { UserService } from '../services/user.service';

@Module({
  imports: [
    RedisModule,
    PassportModule,
    TypeOrmModule.forFeature([UserRepository, RoleRepository, ImageRepository]),
    JwtModule.register({}),
  ],
  controllers: [AuthController],
  providers: [
    RedisService,
    UserService,
    AuthService,
    PasswordService,
    RefreshTokenService,
    JwtStrategy,
    JwtRtStrategy,
    {
      provide: 'APP_GUARD',
      useClass: JwtGuard,
    },
    JwtService,
  ],
  exports: [AuthService, PasswordService],
})
export class AuthModule {}
