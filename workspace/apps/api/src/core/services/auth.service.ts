import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { LoginRequest } from '../../app/dtos/requests/auth/LoginRequest';
import { RegisterRequest } from '../../app/dtos/requests/auth/RegisterRequest';
import { ResetPasswordRequest } from '../../app/dtos/requests/auth/ResetPasswordRequest';
import { BaseResponse } from '../../app/dtos/responses/common/BaseResponse';
import { RoleRepository } from '../../infrastructure/repositories/role.repository';
import { UserRepository } from '../../infrastructure/repositories/user.repository';
import { MailService } from '../../infrastructure/services/mail.service';
import { Constants } from '../common/constants/Contants';
import { RoleEnum } from '../common/enums/RoleEnum';
import { JwtMailConfirmationPayload } from '../common/modals/JwtMailConfirmationPayload';
import { JwtPayload } from '../common/modals/JwtPayload';
import { JwtRtPayload } from '../common/modals/JwtRtPayload';
import { JwtRtReqPayload } from '../common/modals/JwtRtReqPayload';
import { JwtService } from './jwt.service';
import { PasswordService } from './password.service';
import { RefreshTokenService } from './refresh-token.service';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private mailService: MailService,
    private passwordService: PasswordService,
    private refreshTokenService: RefreshTokenService,
    private roleRepository: RoleRepository,
    private userRepository: UserRepository
  ) {}

  public async login(loginRequest: LoginRequest) {
    const user = await this.validateUser(
      loginRequest.email,
      loginRequest.password
    );
    const payload: JwtPayload = {
      email: user.email,
      id: user.id,
      role: user.role.code as RoleEnum,
      firstName: user.firstName,
      lastName: user.lastName,
    };
    const rtPayload: JwtRtPayload = {
      email: user.email,
      id: user.id,
    };
    const [token, refreshToken] = await Promise.all([
      this.jwtService.signToken(payload),
      this.jwtService.signRefreshToken(rtPayload),
    ]);
    await this.refreshTokenService.save(user.id, refreshToken);
    return new BaseResponse({ token, refreshToken });
  }

  @Transactional()
  public async register(registerReq: RegisterRequest) {
    const user = await this.userService.findByEmailWithRole(registerReq.email);
    if (user)
      throw new ConflictException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_CONFLICT,
      });
    const userRole = await this.roleRepository.findOne({
      code: RoleEnum.USER,
    });
    await this.userRepository.save({
      email: registerReq.email,
      password: this.passwordService.hashPassword(registerReq.password),
      firstName: registerReq.firstName,
      lastName: registerReq.lastName,
      role: userRole,
    });
    const confirmationToken = await this.jwtService.signComfimationToken({
      email: registerReq.email,
    });
    try {
      this.mailService.sendUserConfirmation(
        registerReq.email,
        `${registerReq.firstName} ${registerReq.lastName}`,
        confirmationToken
      );
    } catch (e) {
      Logger.error(e);
    }
  }

  public async validateUser(email: string, password: string) {
    const user = await this.userService.findByEmailWithRole(email);
    if (user) {
      if (!this.passwordService.comparePassword(user.password, password)) {
        throw new UnauthorizedException({
          errorCode: Constants.RESPONSE_EXCEPTION_CODE.WRONG_PASSWORD_OR_EMAIL,
        });
      }
      if (user.banned)
        throw new UnauthorizedException({
          errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_BANNED,
        });
      return user;
    }
    throw new UnauthorizedException({
      errorCode: Constants.RESPONSE_EXCEPTION_CODE.WRONG_PASSWORD_OR_EMAIL,
    });
  }

  @Transactional()
  public async refreshToken(reqUser: JwtRtReqPayload) {
    const [user, validRefreshToken] = await Promise.all([
      this.userService.findByEmailWithRole(reqUser.email),
      this.refreshTokenService.validRefreshToken(
        reqUser.id,
        reqUser.refreshToken
      ),
    ]);
    if (user) {
      if (user.banned)
        throw new UnauthorizedException({
          errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_BANNED,
        });
      const payload: JwtPayload = {
        email: user.email,
        id: user.id,
        role: user.role.code as RoleEnum,
        firstName: user.firstName,
        lastName: user.lastName,
      };
      const rtPayload: JwtRtPayload = {
        email: user.email,
        id: user.id,
      };
      const [token, refreshToken] = await Promise.all([
        this.jwtService.signToken(payload),
        this.jwtService.signRefreshToken(rtPayload),
      ]);
      if (validRefreshToken)
        await this.refreshTokenService.replace(
          user.id,
          reqUser.refreshToken,
          refreshToken
        );
      else
        throw new UnauthorizedException({
          errorCode: Constants.RESPONSE_EXCEPTION_CODE.INVALID_CREDENTIALS,
        });
      return new BaseResponse({ token, refreshToken });
    }
    throw new BadRequestException({
      errorCode: Constants.RESPONSE_EXCEPTION_CODE.BAD_REQUEST,
    });
  }

  public async resendConfirmationEmail(reqUser: JwtPayload) {
    const user = await this.userService.findByEmail(reqUser.email);
    if (user.isVerified)
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_VERIFIED,
      });
    const confirmationToken = await this.jwtService.signComfimationToken({
      email: reqUser.email,
    });
    try {
      this.mailService.sendUserConfirmation(
        reqUser.email,
        `${reqUser.firstName} ${reqUser.lastName}`,
        confirmationToken
      );
    } catch (e) {
      Logger.error(e);
    }
  }

  public async confirmEmail(token: string) {
    try {
      const payload = (await this.jwtService.verifyConfirmationToken(
        token
      )) as JwtMailConfirmationPayload;
      const user = await this.userService.findByEmail(payload.email);
      if (user.isVerified) {
        throw new ForbiddenException({
          errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_VERIFIED,
        });
      }
      user.isVerified = true;
      await this.userRepository.save(user);
    } catch (e) {
      throw new BadRequestException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INVALID_CREDENTIALS,
      });
    }
  }

  public async forgotPassword(email: string) {
    const user = await this.userService.findByEmail(email);
    if (!user)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_NOTFOUND,
      });
    if (user.banned) {
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_BANNED,
      });
    }
    const token = await this.jwtService.signForgotPasswordToken(user.email);
    try {
      this.mailService.sendForgotPasswordMail(user.email, token);
    } catch (e) {
      Logger.error(e);
    }
  }

  public async resetPassword({ token, password }: ResetPasswordRequest) {
    let payload;
    try {
      payload = await this.jwtService.verifyForgotPasswordToken(token);
    } catch (e) {
      throw new BadRequestException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INVALID_CREDENTIALS,
      });
    }
    const user = await this.userService.findByEmail(payload.email);
    if (user.banned) {
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.USER_BANNED,
      });
    }
    user.password = this.passwordService.hashPassword(password);
    await this.userRepository.save(user);
  }
}
