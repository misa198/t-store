import { ApiProperty } from '@nestjs/swagger';
import {
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
} from '@workspace/common/constants';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class ForgotPasswordRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.email'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)
  @IsEmail()
  email: string;
}
