import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Product } from './Product';

@Entity({
  name: 'favourite_product',
})
export class FavouriteProduct {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @ManyToOne(() => Product, (product) => product.favouriteProducts, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'product_id',
  })
  product: Product;
}
