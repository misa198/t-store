export class BaseResponse<D, M = void> {
  private data: D;
  private meta?: M;

  constructor(data: D, meta?: M) {
    this.data = data;
    this.meta = meta;
  }
}
