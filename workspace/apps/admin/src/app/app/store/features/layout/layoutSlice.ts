import { createSlice } from '@reduxjs/toolkit';

interface State {
  drawerOpen: boolean;
  pageContainerWidth: number;
}

const initialState: State = {
  drawerOpen: true,
  pageContainerWidth: 0,
};

export const layoutSlice = createSlice({
  name: 'layout',
  initialState,
  reducers: {
    toggleDrawer: (state: State) => {
      state.drawerOpen = !state.drawerOpen;
    },
  },
});

export const { toggleDrawer } = layoutSlice.actions;
export default layoutSlice.reducer;
