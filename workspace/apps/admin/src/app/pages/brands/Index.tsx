import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';
import BrandFilter from '../../components/pages/brands/BrandFilter';
import BrandTable from '../../components/pages/brands/BrandTable';

const BrandsIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('brands.title')} />
      <PageLayout title={t('brands.title')}>
        <BrandFilter />
        <BrandTable />
      </PageLayout>
    </>
  );
};

export default BrandsIndex;
