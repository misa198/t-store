import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from '../../app/controllers/user.controller';
import { ImageRepository } from '../../infrastructure/repositories/image.repository';
import { RoleRepository } from '../../infrastructure/repositories/role.repository';
import { UserRepository } from '../../infrastructure/repositories/user.repository';
import { UserService } from '../services/user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository, RoleRepository, ImageRepository]),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
