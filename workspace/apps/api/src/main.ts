/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { initializeTransactionalContext } from 'typeorm-transactional-cls-hooked';
import { Constants } from './core/common/constants/Contants';
import { AppModule } from './infrastructure/modules/app.module';

async function bootstrap() {
  initializeTransactionalContext();
  const app = await NestFactory.create(AppModule, { cors: true });
  app.useGlobalPipes(
    new ValidationPipe(),
    new ValidationPipe({
      transform: true,
    })
  );

  const config = app.get(ConfigService);
  app.setGlobalPrefix(Constants.API_PREFIX);

  if (config.get(Constants.CONFIG_KEY.NODE_ENV) !== 'production') {
    const swaggerConfig = new DocumentBuilder()
      .setTitle(Constants.API_TITLE)
      .setDescription(Constants.API_DESCRIPTION)
      .setVersion(Constants.API_VERSION)
      .addServer(Constants.API_SWAGGER_SERVER)
      .build();
    const document = SwaggerModule.createDocument(app, swaggerConfig);
    SwaggerModule.setup(Constants.API_PREFIX, app, document);
  }

  const port = config.get(Constants.CONFIG_KEY.PORT);
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${Constants.API_PREFIX}`
  );
}

bootstrap();
