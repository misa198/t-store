import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Image } from '../../core/entities/Image';

@EntityRepository(Image)
export class ImageRepository extends BaseRepository<Image> {}
