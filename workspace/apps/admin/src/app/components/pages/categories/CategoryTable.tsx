import { Box, IconButton } from '@mui/material';
import ModeEditOutlinedIcon from '@mui/icons-material/ModeEditOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { PAGINATION_ROWS_PER_PAGE_OPTIONS } from '../../../constants/common';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import {
  getCategoriesThunk,
  deleteCategoryThunk,
} from '../../../app/store/features/category/categoryThunk';
import {
  Category,
  GetCategoriesIsActive,
  GetCategoriesQuery,
  GetCategoriesSort,
} from '@workspace/react/modals';
import { Link, useSearchParams } from 'react-router-dom';
import {
  CATEGORY_ACTIVE_KEY,
  CATEGORY_SEARCH_KEY,
  CATEGORY_SORT_KEY,
} from '../../../constants/keys';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import Loading from '../../common/Loading';
import { deleteBrandThunk } from '../../../app/store/features/brand/brandThunk';

const CategoryTable: FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const [searchParams] = useSearchParams();
  const categories = useAppSelector((state) => state.category.categories);
  const [pageSize, setPageSize] = useState(PAGINATION_ROWS_PER_PAGE_OPTIONS[0]);
  const [displayedCategories, setDisplayedCategories] = useState<Category[]>(
    []
  );
  const active = searchParams.get(CATEGORY_ACTIVE_KEY);
  const sort = searchParams.get(CATEGORY_SORT_KEY);
  const keyword = searchParams.get(CATEGORY_SEARCH_KEY);

  const onDeleteCategorySlug = (slug: string) => {
    dispatch(deleteCategoryThunk(slug));
  };

  const columns: GridColDef[] = [
    {
      field: 'id',
      headerName: t('categories.table.id'),
      sortable: false,
      disableColumnMenu: true,
      width: 50,
      align: 'center',
      editable: false,
      headerAlign: 'center',
      valueGetter: (params) => params.api.getRowIndex(params.row.id) + 1,
    },
    {
      field: 'name',
      headerName: t('categories.table.name'),
      sortable: false,
      editable: false,
      disableColumnMenu: true,
      flex: 1,
      minWidth: 300,
    },
    {
      field: 'actions',
      headerName: t('categories.table.actions'),
      sortable: false,
      editable: false,
      disableColumnMenu: true,
      align: 'center',
      headerAlign: 'center',
      width: 120,
      renderCell: (params) => (
        <Box>
          <Link to={`/categories/${params.row.slug}`}>
            <IconButton size="small">
              <ModeEditOutlinedIcon fontSize="small" />
            </IconButton>
          </Link>
          <IconButton
            size="small"
            onClick={() => onDeleteCategorySlug(params.row.slug)}
          >
            <DeleteOutlineOutlinedIcon fontSize="small" />
          </IconButton>
        </Box>
      ),
    },
  ];

  useEffect(() => {
    const query: GetCategoriesQuery = {};
    if (active) query.isActive = active as GetCategoriesIsActive;
    if (sort) query.sort = sort as GetCategoriesSort;
    dispatch(getCategoriesThunk(query));
  }, [dispatch, active, sort]);

  useEffect(() => {
    setDisplayedCategories(
      keyword
        ? categories.data.filter((b) =>
            b.name.toLowerCase().includes(keyword.toLowerCase())
          )
        : categories.data
    );
  }, [categories.data, keyword]);

  const onPageSizeChange = (newValue: number) => {
    setPageSize(newValue);
  };

  return (
    <Box sx={{ mt: 4, overflowX: 'auto', maxHeight: '500px' }}>
      <DataGrid
        autoHeight
        columns={columns}
        rows={displayedCategories}
        rowsPerPageOptions={PAGINATION_ROWS_PER_PAGE_OPTIONS}
        disableColumnSelector
        disableSelectionOnClick
        pagination
        pageSize={pageSize}
        onPageSizeChange={onPageSizeChange}
        localeText={{
          MuiTablePagination: {
            labelDisplayedRows: ({ from, to, count }) =>
              t('common.labels.displayedRows', {
                from,
                to,
                count,
              }),
            labelRowsPerPage: t('common.labels.rowsPerPage'),
          },
        }}
        components={{
          LoadingOverlay: Loading,
        }}
        loading={categories.loading}
      />
    </Box>
  );
};

export default CategoryTable;
