import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';

const SettingsIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('settings.title')} />
      <PageLayout title={t('settings.title')}>Settings</PageLayout>
    </>
  );
};

export default SettingsIndex;
