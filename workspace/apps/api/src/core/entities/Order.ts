import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Address } from './Address';
import { OrderItem } from './OrderItem';
import { OrderStatus } from './OrderStatus';
import { Payment } from './Payment';
import { User } from './User';

@Entity({
  name: 'order',
})
export class Order {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'url',
    type: 'varchar',
    length: 4000,
    nullable: false,
  })
  url: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @ManyToOne(() => User, (user) => user.orders, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'user_id',
  })
  user: User;

  @ManyToOne(() => Address, (address) => address.orders, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'address_id',
  })
  address: Address;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order, {
    cascade: ['insert'],
  })
  orderItems: OrderItem[];

  @OneToMany(() => OrderStatus, (orderStatus) => orderStatus.order, {
    cascade: ['insert'],
  })
  orderStatuses: OrderStatus[];

  @OneToOne(() => Payment, (payment) => payment.order, {
    cascade: ['insert'],
  })
  payment: Payment;
}
