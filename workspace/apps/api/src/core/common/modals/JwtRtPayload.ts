import { JwtPayload as DefaultJwtpayload } from 'jsonwebtoken';

export interface JwtRtPayload extends DefaultJwtpayload {
  email: string;
  id: number;
}
