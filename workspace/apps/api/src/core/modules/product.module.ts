import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductController } from '../../app/controllers/product.controller';
import { BrandRepository } from '../../infrastructure/repositories/brand.repository';
import { CategoryRepository } from '../../infrastructure/repositories/category.repository';
import { ImageRepository } from '../../infrastructure/repositories/image.repository';
import { ProductRepository } from '../../infrastructure/repositories/product.repository.s';
import { VariantRepository } from '../../infrastructure/repositories/variant.repository';
import { BrandService } from '../services/brand.service';
import { CategoryService } from '../services/category.service';
import { ProductService } from '../services/product.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProductRepository,
      VariantRepository,
      BrandRepository,
      CategoryRepository,
      ImageRepository,
    ]),
  ],
  controllers: [ProductController],
  providers: [ProductService, BrandService, CategoryService],
  exports: [ProductService],
})
export class ProductModule {}
