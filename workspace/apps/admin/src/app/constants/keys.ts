export const BRAND_SORT_KEY = 'sort';
export const BRAND_ACTIVE_KEY = 'active';
export const BRAND_SEARCH_KEY = 'keyword';

export const CATEGORY_SORT_KEY = 'sort';
export const CATEGORY_ACTIVE_KEY = 'active';
export const CATEGORY_SEARCH_KEY = 'keyword';
