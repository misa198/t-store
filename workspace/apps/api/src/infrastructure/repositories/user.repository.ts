import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { User } from '../../core/entities/User';

@EntityRepository(User)
export class UserRepository extends BaseRepository<User> {}
