import { ROLE_CODE } from '@workspace/react/constants';

export interface Role {
  name: string;
  code: ROLE_CODE;
}
