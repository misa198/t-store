import { Box, Button, Paper, TextField, Typography } from '@mui/material';
import { useEffect, FC } from 'react';
import { useTranslation } from 'react-i18next';
import {
  BRAND_DESCRIPTION_MAX_LENGTH,
  BRAND_NAME_MAX_LENGTH,
} from '@workspace/common/constants';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import { useNavigate } from 'react-router-dom';
import { CreateBrandRequest } from '../../../modals/request/CreateBrandRequest';
import * as yup from 'yup';
import { useFormik } from 'formik';
import { createBrandThunk } from '../../../app/store/features/brand/brandThunk';
import { resetNewBrand } from '../../../app/store/features/brand/brandSlice';

const AddBrand: FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const slug = useAppSelector((state) => state.brand.newBrand.data);

  useEffect(() => {
    dispatch(resetNewBrand());
  }, [dispatch]);

  useEffect(() => {
    if (slug) {
      navigate(`/brands/${slug}`);
    }
  }, [slug]);

  const initialValues: CreateBrandRequest = {
    name: '',
    description: '',
  };

  const validationSchema = yup.object().shape({
    name: yup
      .string()
      .max(
        BRAND_NAME_MAX_LENGTH,
        t('common.validator.max', { max: BRAND_NAME_MAX_LENGTH })
      )
      .required(t('common.validator.required')),
    description: yup
      .string()
      .max(
        BRAND_DESCRIPTION_MAX_LENGTH,
        t('common.validator.max', { max: BRAND_DESCRIPTION_MAX_LENGTH })
      ),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      dispatch(createBrandThunk(values));
    },
  });

  return (
    <Paper
      sx={{
        width: {
          xs: '350px',
          md: '500px',
        },
        padding: 2,
      }}
    >
      <Typography
        variant="h6"
        sx={{
          textAlign: 'center',
          fontWeight: 'bold',
          textTransform: 'uppercase',
        }}
      >
        {t('brands.add.title')}
      </Typography>
      <Box component="form" sx={{ mt: 2 }}>
        <Box>
          <TextField
            variant="outlined"
            fullWidth
            onChange={formik.handleChange}
            value={formik.values.name}
            helperText={formik.errors.name && formik.errors.name}
            error={Boolean(formik.errors.name)}
            name="name"
            label={t('brands.table.name')}
          />
        </Box>
        <Box sx={{ mt: 2 }}>
          <TextField
            multiline
            rows={5}
            fullWidth
            onChange={formik.handleChange}
            value={formik.values.description}
            helperText={formik.errors.description && formik.errors.description}
            error={Boolean(formik.errors.description)}
            name="description"
            label={t('brands.table.description')}
          />
          <Box>
            <Typography
              component="div"
              variant="caption"
              sx={{ textAlign: 'right', mr: 0.5 }}
            >
              {formik.values.description?.length || 0}/
              {BRAND_DESCRIPTION_MAX_LENGTH}
            </Typography>
          </Box>
        </Box>
        <Box sx={{ mt: 2, display: 'flex', justifyContent: 'flex-end' }}>
          <Button
            variant="contained"
            onClick={() => formik.handleSubmit()}
            disabled={!formik.isValid}
          >
            {t('common.btn.save')}
          </Button>
        </Box>
      </Box>
    </Paper>
  );
};

export default AddBrand;
