import { classes } from '@automapper/classes';
import { AutomapperModule } from '@automapper/nestjs';
import { Module } from '@nestjs/common';
import { BrandProfile } from '../common/mappers/brand.profile';
import { CategoryProfile } from '../common/mappers/category.profile';
import { ImageProfile } from '../common/mappers/image.profile';
import { ProductProfile } from '../common/mappers/product.profile';
import { RoleProfile } from '../common/mappers/role.profile';
import { UserProfile } from '../common/mappers/user.profile';

const mapperProfiles = [
  BrandProfile,
  CategoryProfile,
  ImageProfile,
  ProductProfile,
  RoleProfile,
  UserProfile,
];

@Module({
  imports: [
    AutomapperModule.forRoot({
      strategyInitializer: classes(),
    }),
  ],
  providers: [...mapperProfiles],
  exports: [...mapperProfiles],
})
export class MapperModule {}
