import { AutoMap } from '@automapper/classes';

export class CategoryResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  name: string;

  @AutoMap()
  slug: string;
}
