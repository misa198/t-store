import AddRoundedIcon from '@mui/icons-material/AddRounded';
import {
  Box,
  Button,
  FormControl,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  TextField,
} from '@mui/material';
import { ChangeEvent, FC, useEffect, useState } from 'react';
import BasicModal from '../../common/BasicModal';
import { useTranslation } from 'react-i18next';
import { GetBrandsSort } from '@workspace/react/modals';
import { useSearchParams } from 'react-router-dom';
import {
  BRAND_ACTIVE_KEY,
  BRAND_SEARCH_KEY,
  BRAND_SORT_KEY,
} from '../../../constants/keys';
import AddBrand from './AddBrand';

const selectStyles = {
  ml: {
    xs: 0,
    lg: 2,
  },
  mt: {
    xs: 2,
    lg: 0,
  },
  flexGrow: {
    xs: 1,
    lg: 0,
  },
};

interface SortOption {
  label: string;
  value: GetBrandsSort;
}

const sortOptions: SortOption[] = [
  {
    label: 'brands.table.sort.nameAsc',
    value: 'name_asc',
  },
  {
    label: 'brands.table.sort.nameDesc',
    value: 'name_desc',
  },
  {
    label: 'brands.table.sort.timeAsc',
    value: 'time_asc',
  },
  {
    label: 'brands.table.sort.timeDesc',
    value: 'time_desc',
  },
];

interface PublishedOption {
  label: string;
  value: 'true' | 'false';
}

const publishedOptions: PublishedOption[] = [
  {
    label: 'brands.table.published.active',
    value: 'true',
  },
  {
    label: 'brands.table.published.inactive',
    value: 'false',
  },
];

const defaultActive = publishedOptions[0].value;
const defaultSort = sortOptions[0].value;
const defaultKeyword = '';

const BrandFilter: FC = () => {
  const { t } = useTranslation();
  const [searchParams, setSearchParams] = useSearchParams();
  const [active, setActive] = useState(
    searchParams.get(BRAND_ACTIVE_KEY) || defaultActive
  );
  const [sort, setSort] = useState(
    searchParams.get(BRAND_SORT_KEY) || defaultSort
  );
  const [keyword, setKeyword] = useState(
    searchParams.get(BRAND_SEARCH_KEY) || defaultKeyword
  );

  const onPublishedChange = (event: SelectChangeEvent) => {
    setActive(event.target.value);
  };

  const onSortChange = (event: SelectChangeEvent) => {
    setSort(event.target.value);
  };

  const onKeywordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setKeyword(event.target.value);
  };

  useEffect(() => {
    const query: any = {};
    if (sort !== defaultSort) query.sort = sort;
    if (active !== defaultActive) query.active = active;
    if (keyword !== defaultKeyword) query.keyword = keyword;
    setSearchParams(query);
  }, [sort, active, keyword, setSearchParams]);

  return (
    <Paper
      variant="outlined"
      sx={{
        px: 2,
        py: 3,
        display: 'flex',
        flexDirection: {
          xs: 'column',
          lg: 'row',
        },
      }}
    >
      <Box sx={{ flexGrow: 1 }}>
        <TextField
          placeholder="Search something"
          variant="outlined"
          size="small"
          fullWidth
          value={keyword}
          onChange={onKeywordChange}
        />
      </Box>
      <Box
        sx={{
          ...selectStyles,
          minWidth: 200,
        }}
      >
        <FormControl fullWidth size="small">
          <Select value={sort} onChange={onSortChange}>
            {sortOptions.map((option) => (
              <MenuItem key={option.label} value={option.value}>
                {t(option.label)}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <Box
        sx={{
          ...selectStyles,
          minWidth: 150,
        }}
      >
        <FormControl fullWidth size="small">
          <Select value={active} onChange={onPublishedChange}>
            {publishedOptions.map((option) => (
              <MenuItem key={option.label} value={String(option.value)}>
                {t(option.label)}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <BasicModal
        Button={
          <Button
            variant="contained"
            disableElevation
            startIcon={<AddRoundedIcon />}
            sx={{
              ml: {
                xs: 0,
                lg: 2,
              },
              mt: {
                xs: 2,
                lg: 0,
              },
            }}
          >
            {t('brands.addBrand')}
          </Button>
        }
      >
        <AddBrand />
      </BasicModal>
    </Paper>
  );
};

export default BrandFilter;
