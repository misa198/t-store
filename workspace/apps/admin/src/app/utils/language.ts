const LANGUAGE_KEY = 'LANGUAGE';

export const getLanguage = () => {
  const language = localStorage.getItem(LANGUAGE_KEY);
  return language === 'vi' ? 'vi' : 'en';
};

export const toggleLanguage = () => {
  const language = getLanguage();
  localStorage.setItem(LANGUAGE_KEY, language === 'vi' ? 'en' : 'vi');
};
