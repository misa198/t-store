import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';
import { BrandSortEnum } from '../../../../core/common/enums/BrandSortEnum';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class GetBrandsQuery {
  @ApiProperty({
    type: Boolean,
    default: true,
    description: MessageHelper.getMessage('brand.isActive'),
    required: false,
    enum: [true, false]
  })
  @Transform(({ value }) => value == 'true')
  @IsOptional()
  @IsBoolean()
  isActive = true;

  @ApiProperty({
    type: String,
    default: BrandSortEnum.NAME_ASC,
    description: MessageHelper.getMessage('common.sortBy'),
    required: false,
    enum: BrandSortEnum
  })
  @IsOptional()
  @IsEnum(BrandSortEnum)
  sort = BrandSortEnum.NAME_ASC;
}
