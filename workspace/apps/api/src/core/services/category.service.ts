import { Mapper } from '@automapper/core';
import { InjectMapper } from '@automapper/nestjs';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { FindManyOptions, In } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateCategoryRequest } from '../../app/dtos/requests/category/CreateCategoryRequest';
import { GetCategoriesQuery } from '../../app/dtos/requests/category/GetCategoryQuery';
import { UpdateCategoryRequest } from '../../app/dtos/requests/category/UpdateCategoryRequest';
import { CategoryResponse } from '../../app/dtos/responses/category/CategoryResponse';
import { BaseResponse } from '../../app/dtos/responses/common/BaseResponse';
import { CategoryRepository } from '../../infrastructure/repositories/category.repository';
import { Constants } from '../common/constants/Contants';
import { CategorySortEnum } from '../common/enums/CategorySortEnum';
import { RoleEnum } from '../common/enums/RoleEnum';
import { JwtPayload } from '../common/modals/JwtPayload';
import { Utils } from '../common/utils/Utils';
import { Category } from '../entities/Category';
import { CategoryInternalResponse } from '../../app/dtos/responses/category/CategoryInternalResponse';

@Injectable()
export class CategoryService {
  private rolesCanViewInactiveBrand = [RoleEnum.ADMIN, RoleEnum.MANAGER];

  constructor(
    private categoryRepository: CategoryRepository,
    @InjectMapper()
    private readonly mapper: Mapper
  ) {}

  @Transactional()
  public async insertCategory(
    createCategoryRequest: CreateCategoryRequest
  ): Promise<BaseResponse<CategoryResponse>> {
    const newCategory = await this.categoryRepository.save({
      name: createCategoryRequest.name,
      slug: Utils.generateSlug(createCategoryRequest.name),
    });
    return new BaseResponse(
      this.mapper.map(newCategory, Category, CategoryInternalResponse)
    );
  }

  public async getAllCategories(
    query: GetCategoriesQuery,
    reqUser?: JwtPayload
  ): Promise<BaseResponse<CategoryResponse[]>> {
    if (
      !query.isActive &&
      !this.rolesCanViewInactiveBrand.includes(reqUser?.role)
    ) {
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.FORBIDDEN,
      });
    }

    const queryCondition: FindManyOptions<Category> = {};
    queryCondition.where = {
      isActive: query.isActive,
      isValid: true,
    };
    switch (query.sort) {
      case CategorySortEnum.NAME_ASC: {
        queryCondition.order = {
          name: 'ASC',
        };
        break;
      }
      case CategorySortEnum.NAME_DESC: {
        queryCondition.order = {
          name: 'DESC',
        };
        break;
      }
      case CategorySortEnum.TIME_ASC: {
        queryCondition.order = {
          createdAt: 'ASC',
        };
        break;
      }
      case CategorySortEnum.TIME_DESC: {
        queryCondition.order = {
          createdAt: 'DESC',
        };
        break;
      }
    }
    const categories = await this.categoryRepository.find(queryCondition);
    if (Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)) {
      return new BaseResponse(
        this.mapper.mapArray(categories, Category, CategoryInternalResponse)
      );
    }
    return new BaseResponse(
      this.mapper.mapArray(categories, Category, CategoryResponse)
    );
  }

  public async getCategory(
    slug: string,
    reqUser?: JwtPayload
  ): Promise<BaseResponse<CategoryResponse>> {
    const category = await this.findValidCategoryBySlug(slug);
    if (!category)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    if (
      !this.rolesCanViewInactiveBrand.includes(reqUser?.role) &&
      !category.isActive
    )
      throw new ForbiddenException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.FORBIDDEN,
      });
    if (Constants.ROLE_CAN_VIEW_INACTIVE_ITEMS.includes(reqUser?.role)) {
      return new BaseResponse(
        this.mapper.map(category, Category, CategoryInternalResponse)
      );
    }
    return new BaseResponse(
      this.mapper.map(category, Category, CategoryResponse)
    );
  }

  @Transactional()
  public async updateCategory(
    slug: string,
    categoryRequest: UpdateCategoryRequest
  ): Promise<BaseResponse<CategoryResponse>> {
    const category = await this.findValidCategoryBySlug(slug);
    if (!category)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    category.name = categoryRequest.name;
    category.slug = Utils.generateSlug(categoryRequest.name);
    category.isActive = categoryRequest.isActive;
    category.updatedAt = Utils.getDateTime();
    await this.categoryRepository.save(category);
    return new BaseResponse(
      this.mapper.map(category, Category, CategoryInternalResponse)
    );
  }

  @Transactional()
  public async removeCategory(slug: string): Promise<void> {
    const category = await this.findValidCategoryBySlug(slug);
    category.isValid = false;
    category.updatedAt = Utils.getDateTime();
    await this.categoryRepository.save(category);
  }

  public async isValidCategoryIds(ids: number[]) {
    const categories = await this.categoryRepository.find({
      where: {
        id: In(ids),
        isValid: true,
      },
    });
    if (categories.length === ids.length)
      return {
        isValid: true,
        data: categories,
      };
    return {
      isValid: false,
      data: categories,
    };
  }

  private async findValidCategoryBySlug(slug: string) {
    const category = await this.categoryRepository.findOne({
      slug,
      isValid: true,
    });
    if (!category)
      throw new NotFoundException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.NOT_FOUND,
      });
    return category;
  }
}
