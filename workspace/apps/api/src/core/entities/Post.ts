import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Image } from './Image';
import { User } from './User';

@Entity({
  name: 'post',
})
export class Post {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'title',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  title: string;

  @Column({
    name: 'slug',
    type: 'varchar',
    length: 200,
    nullable: false,
    unique: true,
  })
  slug: string;

  @Column({
    name: 'content',
    type: 'text',
    nullable: false,
  })
  content: string;

  @Column({
    name: 'seo_image',
    type: 'varchar',
    length: 4000,
    nullable: false,
  })
  seoImage: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @ManyToOne(() => User, (user) => user.posts, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'user_id',
  })
  user: User;

  @ManyToMany(() => Image, {
    cascade: ['insert'],
  })
  @JoinTable({
    name: 'post_image',
    joinColumn: {
      name: 'post_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'image_id',
      referencedColumnName: 'id',
    },
  })
  images: Image[];
}
