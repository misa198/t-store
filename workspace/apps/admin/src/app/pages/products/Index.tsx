import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';

const ProductsIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('products.title')} />
      <PageLayout title={t('products.title')}>Products</PageLayout>
    </>
  );
};

export default ProductsIndex;
