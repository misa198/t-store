import { AutoMap } from '@automapper/classes';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'category'
})
export class Category {
  @AutoMap()
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint'
  })
  id: number;

  @AutoMap()
  @Column({
    name: 'name',
    type: 'varchar',
    length: 30,
    nullable: false
  })
  name: string;

  @AutoMap()
  @Column({
    name: 'slug',
    type: 'varchar',
    length: 30,
    nullable: false,
    unique: true
  })
  slug: string;

  @Column({
    name: 'is_active',
    type: 'boolean',
    nullable: false,
    default: true
  })
  @AutoMap()
  isActive: boolean;

  @AutoMap()
  @Column({
    name: 'is_valid',
    type: 'boolean',
    nullable: false,
    default: true
  })
  isValid: boolean;

  @AutoMap()
  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date;

  @AutoMap()
  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP'
  })
  updatedAt: Date;
}
