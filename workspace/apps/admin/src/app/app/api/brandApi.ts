import qs from 'query-string';
import { api } from './axios';
import {
  BaseResponse,
  Brand,
  BrandDetails,
  GetBrandsQuery,
} from '@workspace/react/modals';
import { UpdateBrandRequest } from '../../modals/request/UpdateBrandRequest';
import { CreateBrandRequest } from '../../modals/request/CreateBrandRequest';

const BRANDS_URL = '/brands';

export const getBrandsApi = async (
  params: GetBrandsQuery
): Promise<BaseResponse<Brand[]>> =>
  api.get(
    qs.stringifyUrl({
      url: BRANDS_URL,
      query: { ...params },
    })
  );

export const getBrandApi = async (
  slug: string
): Promise<BaseResponse<BrandDetails>> => api.get(`${BRANDS_URL}/${slug}`);

export const updateBrandApi = async (
  slug: string,
  reqBody: UpdateBrandRequest
): Promise<BaseResponse<BrandDetails>> =>
  api.put(`${BRANDS_URL}/${slug}`, reqBody);

export const createBrandApi = async (
  reqBody: CreateBrandRequest
): Promise<BaseResponse<BrandDetails>> => api.post(BRANDS_URL, reqBody);

export const deleteBrandApi = async (
  slug: string
): Promise<BaseResponse<void>> => api.delete(`${BRANDS_URL}/${slug}`);
