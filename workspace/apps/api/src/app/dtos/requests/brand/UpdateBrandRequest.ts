import { ApiProperty } from '@nestjs/swagger';
import { BRAND_NAME_MAX_LENGTH } from '@workspace/common/constants';
import { IsBoolean, IsNotEmpty, IsString, Length } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class UpdateBrandRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('brand.name'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(0, BRAND_NAME_MAX_LENGTH)
  name: string;

  @ApiProperty({
    type: Boolean,
    required: true,
    description: MessageHelper.getMessage('brand.isActive'),
  })
  @IsNotEmpty()
  @IsBoolean()
  isActive: boolean;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('brand.description'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(0, 4000)
  description: string;
}
