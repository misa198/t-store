import { AutoMap } from '@automapper/classes';
import { BrandResponse } from '../brand/BrandResponse';
import { CategoryResponse } from '../category/CategoryResponse';
import { VariantResponse } from './VariantResponse';
import { ProductImageResponse } from './ProductImageResponse';

export class ProductInternalResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  name: string;

  @AutoMap()
  slug: string;

  @AutoMap()
  shortDescription: string;

  @AutoMap()
  description: string;

  @AutoMap()
  favouriteCount: number;

  @AutoMap()
  rateCount: number;

  @AutoMap()
  averageRate: number;

  @AutoMap()
  isActive: boolean;

  @AutoMap(() => [CategoryResponse])
  categories: CategoryResponse[];

  @AutoMap(() => [BrandResponse])
  brand: BrandResponse;

  @AutoMap(() => [VariantResponse])
  variants: VariantResponse[];

  @AutoMap(() => [ProductImageResponse])
  productImages: ProductImageResponse[];
}
