import { Injectable } from '@nestjs/common';
import bcrypt from 'bcryptjs';
import { Constants } from '../common/constants/Contants';

@Injectable()
export class PasswordService {
  public hashPassword(password: string) {
    const salt = bcrypt.genSaltSync(Constants.PASSWORD_SALT_LENGTH);
    return bcrypt.hashSync(password, salt);
  }

  public comparePassword(password: string, input: string) {
    return bcrypt.compareSync(input, password);
  }
}
