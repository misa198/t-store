import { createMap, Mapper } from '@automapper/core';
import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Injectable } from '@nestjs/common';
import { BrandInternalResponse } from '../../../app/dtos/responses/brand/BrandInternalResponse';
import { BrandResponse } from '../../../app/dtos/responses/brand/BrandResponse';
import { Brand } from '../../entities/Brand';

@Injectable()
export class BrandProfile extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile() {
    return (mapper) => {
      createMap(mapper, Brand, BrandResponse);
      createMap(mapper, Brand, BrandInternalResponse);
    };
  }
}
