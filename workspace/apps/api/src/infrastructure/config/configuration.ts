import { environment } from '../../environments/environment';

export const configuration = () => ({
  ...environment,
});
