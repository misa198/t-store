import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
  Length,
  Min,
} from 'class-validator';
import { VARIANT_NAME_MAX_LENGTH } from 'libs/common/constants/src/lib/validations';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class VariantRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('variant.name'),
    maxLength: VARIANT_NAME_MAX_LENGTH,
  })
  @IsString()
  @IsNotEmpty()
  @Length(1, VARIANT_NAME_MAX_LENGTH)
  name: string;

  @ApiProperty({
    type: Number,
    required: true,
    description: MessageHelper.getMessage('variant.price'),
  })
  @IsNumber()
  @IsNotEmpty()
  @Min(0)
  @Transform(({ value }) => parseFloat(value))
  price: number;

  @ApiProperty({
    type: Number,
    required: true,
    description: MessageHelper.getMessage('variant.quantity'),
  })
  @IsInt()
  @IsNotEmpty()
  @Min(0)
  quantity: number;
}
