import { AutoMap } from '@automapper/classes';

export class ImageResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  url: string;
}
