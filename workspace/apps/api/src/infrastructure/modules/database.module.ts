import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Constants } from '../../core/common/constants/Contants';
import { Address } from '../../core/entities/Address';
import { Banner } from '../../core/entities/Banner';
import { Brand } from '../../core/entities/Brand';
import { Category } from '../../core/entities/Category';
import { FavouriteProduct } from '../../core/entities/FavouriteProduct';
import { Image } from '../../core/entities/Image';
import { Order } from '../../core/entities/Order';
import { OrderItem } from '../../core/entities/OrderItem';
import { OrderStatus } from '../../core/entities/OrderStatus';
import { Payment } from '../../core/entities/Payment';
import { PaymentMethod } from '../../core/entities/PaymentMethod';
import { Post } from '../../core/entities/Post';
import { Product } from '../../core/entities/Product';
import { ProductImage } from '../../core/entities/ProductImage';
import { Rate } from '../../core/entities/Rate';
import { Role } from '../../core/entities/Role';
import { Setting } from '../../core/entities/Setting';
import { Status } from '../../core/entities/Status';
import { User } from '../../core/entities/User';
import { Variant } from '../../core/entities/Variant';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigService],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        type: 'postgres',
        host: config.get(Constants.CONFIG_KEY.DB_HOST),
        port: config.get(Constants.CONFIG_KEY.DB_PORT),
        username: config.get(Constants.CONFIG_KEY.DB_USER),
        password: config.get(Constants.CONFIG_KEY.DB_PASSWORD),
        database: config.get(Constants.CONFIG_KEY.DB_NAME),
        logging: false,
        autoLoadEntities: true,
        entities: [
          Address,
          Banner,
          Brand,
          Category,
          FavouriteProduct,
          Image,
          Order,
          OrderItem,
          OrderStatus,
          Payment,
          PaymentMethod,
          Post,
          Product,
          ProductImage,
          Rate,
          Role,
          Setting,
          Status,
          User,
          Variant,
        ],
      }),
    }),
  ],
  providers: [],
  exports: [],
})
export class DatabaseModule {}
