import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';

const OrdersIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('orders.title')} />
      <PageLayout title={t('orders.title')}>Orders</PageLayout>
    </>
  );
};

export default OrdersIndex;
