import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BrandController } from '../../app/controllers/brand.controller';
import { BrandRepository } from '../../infrastructure/repositories/brand.repository';
import { ImageRepository } from '../../infrastructure/repositories/image.repository';
import { BrandService } from '../services/brand.service';

@Module({
  imports: [TypeOrmModule.forFeature([BrandRepository, ImageRepository])],
  controllers: [BrandController],
  providers: [BrandService],
  exports: [BrandService],
})
export class BrandModule {}
