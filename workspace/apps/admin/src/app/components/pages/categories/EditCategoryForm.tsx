import { FC, useEffect, memo } from 'react';
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import { CategoryDetails } from '@workspace/react/modals';
import * as yup from 'yup';
import {
  CATEGORY_DESCRIPTION_MAX_LENGTH,
  CATEGORY_NAME_MAX_LENGTH,
} from '@workspace/common/constants';
import { useTranslation } from 'react-i18next';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import { useFormik } from 'formik';
import { UpdateCategoryRequest } from '../../../modals/request/UpdateCategoryRequest';
import { updateCategoryThunk } from '../../../app/store/features/category/categoryThunk';
import { useNavigate } from 'react-router-dom';

interface Props {
  category: CategoryDetails;
}

const EditCategoryForm: FC<Props> = ({ category }) => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const isEdited = useAppSelector((state) => state.category.category.isEdited);

  const initialValues: UpdateCategoryRequest = {
    name: category.name,
    description: category.description,
    isActive: category.isActive as boolean,
  };

  const validationSchema = yup.object().shape({
    name: yup
      .string()
      .max(
        CATEGORY_NAME_MAX_LENGTH,
        t('common.validator.max', { max: CATEGORY_NAME_MAX_LENGTH })
      )
      .required(t('common.validator.required')),
    description: yup
      .string()
      .max(
        CATEGORY_DESCRIPTION_MAX_LENGTH,
        t('common.validator.max', { max: CATEGORY_DESCRIPTION_MAX_LENGTH })
      ),
    isActive: yup.boolean().required(t('common.validator.required')),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      dispatch(
        updateCategoryThunk({
          slug: category.slug,
          category: values,
        })
      );
    },
  });

  useEffect(() => {
    if (isEdited) {
      navigate(`/categories/${category.slug}`);
    }
  }, [category.slug]);

  return (
    <Box component="form">
      <Box>
        <TextField
          variant="outlined"
          fullWidth
          onChange={formik.handleChange}
          value={formik.values.name}
          helperText={formik.errors.name && formik.errors.name}
          error={Boolean(formik.errors.name)}
          name="name"
          label={t('categories.table.name')}
        />
      </Box>
      <Box sx={{ mt: 2 }}>
        <TextField
          multiline
          rows={5}
          fullWidth
          onChange={formik.handleChange}
          value={formik.values.description}
          helperText={formik.errors.description && formik.errors.description}
          error={Boolean(formik.errors.description)}
          name="description"
          label={t('categories.table.description')}
        />
        <Box>
          <Typography
            component="div"
            variant="caption"
            sx={{ textAlign: 'right', mr: 0.5 }}
          >
            {formik.values.description?.length || 0}/
            {CATEGORY_DESCRIPTION_MAX_LENGTH}
          </Typography>
        </Box>
      </Box>
      <Box sx={{ mt: 2 }}>
        <FormControl fullWidth>
          <InputLabel id="is-active">Active</InputLabel>
          <Select
            labelId="is-active"
            label="Active"
            fullWidth
            value={formik.values.isActive ? 'true' : 'false'}
            onChange={(e) => {
              formik.setFieldValue('isActive', e.target.value === 'true');
            }}
          >
            <MenuItem value="true">
              {t('categories.table.published.active')}
            </MenuItem>
            <MenuItem value="false">
              {t('categories.table.published.inactive')}
            </MenuItem>
          </Select>
        </FormControl>
      </Box>
      <Box sx={{ mt: 2, display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          onClick={() => formik.handleSubmit()}
          disabled={!formik.isValid}
        >
          {t('common.btn.save')}
        </Button>
      </Box>
    </Box>
  );
};

export default memo(EditCategoryForm);
