import { RoleEnum } from '../enums/RoleEnum';

export class Constants {
  public static CONFIG_KEY = {
    NODE_ENV: 'nodeEnv',
    PORT: 'port',

    JWT_SECRET_KEY: 'jwtSecretKey',
    JWT_EXPIRES_IN: 'jwtExpiresIn',
    JWT_RT_SECRET_KEY: 'jwtRtSecretKey',
    JWT_RT_EXPIRES_IN: 'jwtRtExpiresIn',
    JWT_MAIL_CONFIRMATION_SECRET_KEY: 'jwtMailConfirmationSecret',
    JWT_MAIL_CONFIRMATION_EXPIRES_IN: 'jwtMailConfirmationExpiresIn',
    JWT_MAIL_FORGOT_PASSWORD_SECRET_KEY: 'jwtMailForgotPasswordSecret',
    JWT_MAIL_FORGOT_PASSWORD_EXPIRES_IN: 'jwtMailForgotPasswordExpiresIn',

    MAIL_USER: 'mailUser',
    MAIL_PASSWORD: 'mailPassword',
    MAIL_BASE_URL: 'mailBaseUrl',

    DB_HOST: 'dbHost',
    DB_PORT: 'dbPort',
    DB_NAME: 'dbName',
    DB_USER: 'dbUser',
    DB_PASSWORD: 'dbPassword',

    REDIS_HOST: 'redisHost',
    REDIS_PORT: 'redisPort',
    REDIS_TTL: 'redisTtl',

    CLOUDINARY_CLOUD_NAME: 'cloudinaryCloudName',
    CLOUDINARY_API_KEY: 'cloudinaryApiKey',
    CLOUDINARY_API_SECRET: 'cloudinaryApiSecret',
    CLOUDINARY_FOLDER: 'cloudinaryFolder',
  };

  public static RESPONSE_EXCEPTION_CODE = {
    EMAIL_NOT_VERIFIED: 'EMAIL_NOT_VERIFIED',
    WRONG_PASSWORD_OR_EMAIL: 'WRONG_PASSWORD_OR_EMAIL',
    INVALID_CREDENTIALS: 'INVALID_CREDENTIALS',
    USER_CONFLICT: 'USER_CONFLICT',
    USER_BANNED: 'USER_BANNED',
    USER_NOT_VERIFIED: 'USER_NOT_VERIFIED',
    USER_VERIFIED: 'USER_VERIFIED',
    USER_NOTFOUND: 'USER_NOTFOUND',
    NOT_FOUND: 'NOT_FOUND',
    BAD_REQUEST: 'BAD_REQUEST',
    FORBIDDEN: 'FORBIDDEN',
    INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
  };

  public static JWT_STRATEGY_NAME = 'jwt';
  public static JWT_RT_STRATEGY_NAME = 'jwt-refresh-token';
  public static MAIL_SYSTEM_NAME = 'T-Store';
  public static RT_REDIX_PREFIX = 'user';

  public static API_TITLE = 'T-Store API';
  public static API_DESCRIPTION = 'The T-Store API description';
  public static API_VERSION = '1.0';
  public static API_PREFIX = 'api';
  public static API_SWAGGER_SERVER = '/';

  public static PASSWORD_SALT_LENGTH = 10;
  public static RT_KEY_LENGTH = 128;
  public static SLUG_ID_LENGTH = 5;
  public static MAX_IMAGE_SIZE = 2 * 1024 * 1024; // 2Mb
  public static REGEX = {
    IMAGE: /\.(jpg|jpeg|png)$/,
  };

  public static DEFAULT_PAGINATION_LIMIT = 12;
  public static MAX_PAGINATION_LIMIT = 50;

  public static ROLE_CAN_VIEW_INACTIVE_ITEMS = [
    RoleEnum.ADMIN,
    RoleEnum.MANAGER,
  ];
}
