import { AutoMap } from '@automapper/classes';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Image } from './Image';
import { Product } from './Product';

@Entity({
  name: 'brand',
})
export class Brand {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  @AutoMap()
  id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  @AutoMap()
  name: string;

  @Column({
    name: 'slug',
    type: 'varchar',
    length: 50,
    nullable: false,
    unique: true,
  })
  @AutoMap()
  slug: string;

  @Column({
    name: 'description',
    type: 'varchar',
    length: 4000,
    nullable: false,
  })
  @AutoMap()
  description: string;

  @Column({
    name: 'is_active',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  @AutoMap()
  isActive: boolean;

  @Column({
    name: 'is_valid',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  @AutoMap()
  isValid: boolean;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  @AutoMap()
  createdAt: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  @AutoMap()
  updatedAt: Date;

  @OneToMany(() => Product, (product) => product.brand, {
    cascade: ['insert'],
  })
  @AutoMap(() => [Product])
  products: Product[];

  @OneToOne(() => Image, (image) => image.brand, {
    cascade: ['insert'],
    nullable: true,
  })
  @JoinColumn({
    name: 'image_id',
  })
  @AutoMap(() => [Image])
  image: Image;
}
