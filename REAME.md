# Nghiên cứu tốt nghiệp

## Xây dựng website thương mại điện tử

### Yêu cầu

- Docker
- Docker Compose

### Cài đặt

- Mở terminal tại thư mục "docker"
- Chạy câu lệnh sau để build và run docker container

```bash
$ chmod +X ./app.sh
$ ./app.sh dev
```

- Sau khi container chạy thành công, chạy `docker ps` để lấy container_id. Chạy tiếp câu lệnh sau để vào `bash` của container.

```bash
$ ./app.sh sh {container_id}
```

- Chạy câu lệnh sau để cài đặt các phụ thuộc

```bash
$ yarn install
```

- Chạy câu lệnh sau để start api server:

```bash
$ yarn start api
```

Truy cập http://localhost:8080/api để xem danh sách các API và các query params tương ứng.

- Chạy câu lệnh sau để start UI phần quản lý:

```bash
$ yarn start api
```

Truy cập http://localhost:5000
