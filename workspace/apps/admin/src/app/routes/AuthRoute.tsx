import { FC, LazyExoticComponent, Suspense } from 'react';

interface Props {
  // eslint-disable-next-line react/no-unused-prop-types
  children: LazyExoticComponent<FC>;
}

const AuthRoute: FC<Props> = (props) => (
  <Suspense fallback={<></>}>{<props.children />}</Suspense>
);

export default AuthRoute;
