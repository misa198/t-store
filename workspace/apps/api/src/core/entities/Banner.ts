import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Image } from './Image';

@Entity({
  name: 'banner'
})
export class Banner {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint'
  })
  id: number;

  @Column({
    name: 'url',
    type: 'varchar',
    length: 4000,
    nullable: false
  })
  url: string;

  @Column({
    name: 'order',
    type: 'int',
    nullable: false
  })
  order: number;

  @OneToOne(() => Image, (image) => image.banner, {
    cascade: ['insert']
  })
  @JoinColumn({
    name: 'image_id'
  })
  image: Image;
}
