import { Injectable } from '@nestjs/common';
import { RedisService } from '../../infrastructure/services/redis.service';
import { Constants } from '../common/constants/Contants';

@Injectable()
export class RefreshTokenService {
  constructor(private redisService: RedisService) {}

  public async validRefreshToken(userId: number, refreshToken: string) {
    const refreshTokens = await this.findOne(userId, refreshToken);
    return Boolean(refreshTokens);
  }

  public async findAllByUserId(userId: number) {
    const refreshTokens = await this.redisService.keys(
      `${Constants.RT_REDIX_PREFIX}_${userId.toString()}_*`
    );
    return refreshTokens ? (refreshTokens as string[]) : null;
  }

  public async findOne(userId: number, refreshToken: string) {
    const refreshTokens = await this.redisService.get(
      `${Constants.RT_REDIX_PREFIX}_${userId.toString()}_${refreshToken}`
    );
    return refreshTokens ? (refreshTokens as string) : null;
  }

  public async replace(userId: number, oldToken: string, newToken: string) {
    return Promise.all([
      await this.redisService.del(
        `${Constants.RT_REDIX_PREFIX}_${userId}_${oldToken}`
      ),
      await this.save(userId, newToken),
    ]);
  }

  public async save(userId: number, token: string) {
    return this.redisService.set(
      `${Constants.RT_REDIX_PREFIX}_${userId}_${token}`,
      token
    );
  }
}
