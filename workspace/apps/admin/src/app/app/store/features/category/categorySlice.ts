import { createSlice } from '@reduxjs/toolkit';
import { Category, CategoryDetails } from '@workspace/react/modals';
import {
  createCategoryThunk,
  getCategoriesThunk,
  getCategoryThunk,
  updateCategoryThunk,
  deleteCategoryThunk,
} from './categoryThunk';

interface State {
  categories: {
    data: Category[];
    loading: boolean;
  };
  category: {
    data: CategoryDetails | null;
    loading: boolean;
    error: boolean;
    isEdited: boolean;
  };
  newCategory: {
    data: string;
    loading: boolean;
    error: boolean;
  };
  deleteCategory: {
    loading: boolean;
    error: boolean;
  };
}

const initialState: State = {
  categories: {
    data: [],
    loading: false,
  },
  category: {
    data: null,
    loading: false,
    error: false,
    isEdited: false,
  },
  newCategory: {
    data: '',
    loading: false,
    error: false,
  },
  deleteCategory: {
    loading: false,
    error: false,
  },
};

const categorySlice = createSlice({
  name: 'brand',
  initialState,
  reducers: {
    resetNewCategory: (state) => {
      state.newCategory.data = '';
      state.newCategory.loading = false;
      state.newCategory.error = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getCategoriesThunk.pending, (state) => {
      state.categories.loading = true;
      state.categories.data = [];
    });
    builder.addCase(getCategoriesThunk.fulfilled, (state, action) => {
      state.categories.loading = false;
      state.categories.data = action.payload;
    });

    builder.addCase(getCategoryThunk.pending, (state) => {
      state.category.data = null;
      state.category.loading = true;
      state.category.error = false;
      state.category.isEdited = false;
    });
    builder.addCase(getCategoryThunk.fulfilled, (state, action) => {
      state.category.data = action.payload;
      state.category.loading = false;
      state.category.error = false;
    });
    builder.addCase(getCategoryThunk.rejected, (state) => {
      state.category.loading = false;
      state.category.error = true;
    });

    builder.addCase(updateCategoryThunk.pending, (state) => {
      state.category.loading = true;
      state.category.error = false;
    });
    builder.addCase(updateCategoryThunk.fulfilled, (state, action) => {
      state.category.data!.slug = action.payload.slug;
      state.category.loading = false;
      state.category.error = false;
      state.category.isEdited = true;
    });
    builder.addCase(updateCategoryThunk.rejected, (state) => {
      state.category.loading = false;
      state.category.error = true;
    });

    builder.addCase(createCategoryThunk.pending, (state) => {
      state.newCategory.loading = true;
      state.newCategory.error = false;
    });
    builder.addCase(createCategoryThunk.fulfilled, (state, action) => {
      state.newCategory.data = action.payload.slug;
      state.newCategory.loading = false;
      state.newCategory.error = false;
    });
    builder.addCase(createCategoryThunk.rejected, (state) => {
      state.newCategory.loading = false;
      state.newCategory.error = true;
    });

    builder.addCase(deleteCategoryThunk.pending, (state) => {
      state.deleteCategory.loading = true;
      state.deleteCategory.error = false;
    });
    builder.addCase(deleteCategoryThunk.fulfilled, (state, action) => {
      state.deleteCategory.loading = false;
      state.deleteCategory.error = false;
      state.categories.data = state.categories.data.filter(
        (category) => category.slug !== action.payload
      );
    });
    builder.addCase(deleteCategoryThunk.rejected, (state) => {
      state.deleteCategory.loading = false;
      state.deleteCategory.error = true;
    });
  },
});

export default categorySlice.reducer;
export const { resetNewCategory } = categorySlice.actions;
