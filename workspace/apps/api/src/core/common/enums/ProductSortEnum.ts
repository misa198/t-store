export enum ProductSortEnum {
  NAME_ASC = 'name_asc',
  NAME_DESC = 'name_desc',
  TIME_ASC = 'time_asc',
  TIME_DESC = 'time_desc',
  PRICE_ASC = 'price_asc',
  PRICE_DESC = 'price_desc',
  BEST_SELLER = 'best_seller',
}
