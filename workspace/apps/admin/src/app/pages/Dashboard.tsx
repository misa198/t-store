import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../components/common/Helmet';
import PageLayout from '../components/common/PageLayout';

const Dashboard: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('dashboard.title')} />
      <PageLayout title={t('dashboard.title')}>Dashboard</PageLayout>
    </>
  );
};

export default Dashboard;
