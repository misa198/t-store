import {
  Controller,
  Delete,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { Constants } from '../../core/common/constants/Contants';
import { FileHandler } from '../../core/common/helpers/FileHelper';
import { JwtPayload } from '../../core/common/modals/JwtPayload';
import { AuthGuard } from '../../core/security/auth.guard';
import { ReqUser } from '../../core/security/req-user.decorator';
import { UserService } from '../../core/services/user.service';

@ApiTags('users')
@Controller('users')
@UseGuards(AuthGuard)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Put('/profile-picture')
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: FileHandler.imageFileFilter,
      limits: {
        fileSize: Constants.MAX_IMAGE_SIZE,
      },
    })
  )
  public async updateProfilePicture(
    @UploadedFile() file: Express.Multer.File,
    @ReqUser() reqUser: JwtPayload
  ) {
    return this.userService.updateProfilePicture(reqUser.id, file);
  }

  @Delete('/profile-picture')
  public async deleteProfilePicture(@ReqUser() reqUser: JwtPayload) {
    return this.userService.deleteProfilePicture(reqUser.id);
  }
}
