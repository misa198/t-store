import { FC, LazyExoticComponent, Suspense } from 'react';
import { ROLE_CODE } from '@workspace/react/constants';

interface Props {
  permissions?: ROLE_CODE[];
  children: LazyExoticComponent<FC>;
}

const PublicRoute: FC<Props> = (props) => {
  return (
    <Suspense fallback={<></>}>
      <props.children />
    </Suspense>
  );
};

export default PublicRoute;
