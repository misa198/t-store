import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RoleEnum } from '../../core/common/enums/RoleEnum';
import { JwtPayload } from '../../core/common/modals/JwtPayload';
import { ReqUser } from '../../core/security/req-user.decorator';
import { Roles } from '../../core/security/roles.decorator';
import { CategoryService } from '../../core/services/category.service';
import { CreateCategoryRequest } from '../dtos/requests/category/CreateCategoryRequest';
import { GetCategoriesQuery } from '../dtos/requests/category/GetCategoryQuery';
import { UpdateCategoryRequest } from '../dtos/requests/category/UpdateCategoryRequest';

@ApiTags('categories')
@Controller('categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post()
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async createCategory(
    @Body() createCategoryRequest: CreateCategoryRequest
  ) {
    return this.categoryService.insertCategory(createCategoryRequest);
  }

  @Get()
  public async getAllCategories(
    @Query() query: GetCategoriesQuery,
    @ReqUser() reqUser?: JwtPayload
  ) {
    return this.categoryService.getAllCategories(query, reqUser);
  }

  @Get(':slug')
  public async getCategory(
    @Param('slug') slug,
    @ReqUser() reqUser?: JwtPayload
  ) {
    return this.categoryService.getCategory(slug, reqUser);
  }

  @Put(':slug')
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async updateCategory(
    @Param('slug') slug,
    @Body() updateCategoryRequest: UpdateCategoryRequest
  ) {
    return this.categoryService.updateCategory(slug, updateCategoryRequest);
  }

  @Delete(':slug')
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async deleteCategory(@Param('slug') slug) {
    return this.categoryService.removeCategory(slug);
  }
}
