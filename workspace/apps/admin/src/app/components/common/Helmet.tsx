import { FC } from 'react';
import { Helmet } from 'react-helmet-async';

interface Props {
  title: string;
}

const HelmetComponent: FC<Props> = ({ title }) => (
  <Helmet>
    <title>{title} | Tstore</title>
  </Helmet>
);

export default HelmetComponent;
