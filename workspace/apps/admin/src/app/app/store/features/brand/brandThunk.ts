import { createAsyncThunk } from '@reduxjs/toolkit';
import { GetBrandsQuery } from '@workspace/react/modals';
import {
  createBrandApi,
  getBrandApi,
  getBrandsApi,
  updateBrandApi,
  deleteBrandApi,
} from '../../../api/brandApi';
import { toast } from '../toast/toastSlice';
import { UpdateCategoryRequest } from '../../../../modals/request/UpdateCategoryRequest';
import { CreateBrandRequest } from '../../../../modals/request/CreateBrandRequest';

export const getBrandsThunk = createAsyncThunk(
  'brand/getBrands',
  async (query: GetBrandsQuery, { dispatch }) => {
    try {
      const { data } = await getBrandsApi(query);
      return data;
    } catch (e) {
      dispatch(
        toast({
          variant: 'error',
          message: 'common.errors.index',
        })
      );
      throw e;
    }
  }
);

interface UpdateBrandThunkPayload {
  slug: string;
  brand: UpdateCategoryRequest;
}

export const updateBrandThunk = createAsyncThunk(
  'brand/updateBrand',
  async (payload: UpdateBrandThunkPayload) => {
    const { data } = await updateBrandApi(payload.slug, payload.brand);
    return data;
  }
);

export const getBrandThunk = createAsyncThunk(
  'brand/getBrand',
  async (slug: string) => {
    const { data } = await getBrandApi(slug);
    return data;
  }
);

export const createBrandThunk = createAsyncThunk(
  'brand/createBrand',
  async (brand: CreateBrandRequest) => {
    const { data } = await createBrandApi(brand);
    return data;
  }
);

export const deleteBrandThunk = createAsyncThunk(
  'brand/deleteBrand',
  async (slug: string) => {
    await deleteBrandApi(slug);
    return slug;
  }
);
