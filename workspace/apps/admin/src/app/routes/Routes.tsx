import { FC, lazy } from 'react';
import { useRoutes } from 'react-router-dom';
import { RouteObject } from 'react-router/lib/router';
import { ROLE_CODE } from '@workspace/react/constants';
import AuthRoute from './AuthRoute';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

const LoginPage = lazy(() => import('../pages/auth/Login'));

export const routes: RouteObject[] = [
  // private routes
  {
    path: '/',
    element: (
      <PrivateRoute
        children={lazy(
          () => import('../components/layouts/DefaultLayout/DefaultLayout')
        )}
      />
    ),
    children: [
      {
        path: '',
        element: (
          <PrivateRoute children={lazy(() => import('../pages/Index'))} />
        ),
      },
      {
        path: 'dashboard',
        element: (
          <PrivateRoute
            children={lazy(() => import('../pages/Dashboard'))}
            permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
          />
        ),
      },
      {
        path: 'brands',
        children: [
          {
            path: '',
            element: (
              <PrivateRoute
                children={lazy(() => import('../pages/brands/Index'))}
                permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
              />
            ),
          },
          {
            path: ':slug',
            element: (
              <PrivateRoute
                children={lazy(() => import('../pages/brands/Edit'))}
                permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
              />
            ),
          },
        ],
      },
      {
        path: 'categories',
        children: [
          {
            path: '',
            element: (
              <PrivateRoute
                children={lazy(() => import('../pages/categories/Index'))}
                permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
              />
            ),
          },
          {
            path: ':slug',
            element: (
              <PrivateRoute
                children={lazy(() => import('../pages/categories/Edit'))}
                permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
              />
            ),
          },
        ],
      },
      {
        path: 'products',
        element: (
          <PrivateRoute
            children={lazy(() => import('../pages/products/Index'))}
            permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
          />
        ),
      },
      {
        path: 'orders',
        element: (
          <PrivateRoute
            children={lazy(() => import('../pages/orders/Index'))}
            permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
          />
        ),
      },
      {
        path: 'customers',
        element: (
          <PrivateRoute
            children={lazy(() => import('../pages/customers/Index'))}
            permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
          />
        ),
      },
      {
        path: 'posts',
        element: (
          <PrivateRoute children={lazy(() => import('../pages/posts/Index'))} />
        ),
      },
      {
        path: 'settings',
        element: (
          <PrivateRoute
            children={lazy(() => import('../pages/settings/Index'))}
            permissions={[ROLE_CODE.ADMIN, ROLE_CODE.MANAGER]}
          />
        ),
      },
      {
        path: '403',
        element: (
          <PrivateRoute children={lazy(() => import('../pages/Page403'))} />
        ),
      },
    ],
  },

  // auth routes
  {
    path: 'auth',
    element: (
      <AuthRoute
        children={lazy(
          () => import('../components/layouts/AuthLayout/AuthLayout')
        )}
      />
    ),
    children: [
      {
        path: 'login',
        element: <LoginPage />,
      },
    ],
  },

  // public routes
  {
    path: '*',
    element: <PublicRoute children={lazy(() => import('../pages/Page404'))} />,
  },
];

const Router: FC = () => {
  const element = useRoutes(routes);

  return <>{element}</>;
};

export default Router;
