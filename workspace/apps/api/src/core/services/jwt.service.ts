import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService as NestJwtService } from '@nestjs/jwt';
import { Constants } from '../common/constants/Contants';
import { JwtMailConfirmationPayload } from '../common/modals/JwtMailConfirmationPayload';
import { JwtPayload } from '../common/modals/JwtPayload';
import { JwtRtPayload } from '../common/modals/JwtRtPayload';

@Injectable()
export class JwtService {
  constructor(
    private config: ConfigService,
    private jwtService: NestJwtService
  ) {}

  public async signToken(payload: JwtPayload) {
    return this.jwtService.signAsync(payload, {
      secret: this.config.get(Constants.CONFIG_KEY.JWT_SECRET_KEY),
      expiresIn: this.config.get(Constants.CONFIG_KEY.JWT_EXPIRES_IN),
    });
  }

  public async signRefreshToken(payload: JwtRtPayload) {
    return this.jwtService.signAsync(payload, {
      secret: this.config.get(Constants.CONFIG_KEY.JWT_RT_SECRET_KEY),
      expiresIn: this.config.get(Constants.CONFIG_KEY.JWT_RT_EXPIRES_IN),
    });
  }

  public async signComfimationToken(payload: JwtMailConfirmationPayload) {
    return this.jwtService.signAsync(payload, {
      secret: this.config.get(
        Constants.CONFIG_KEY.JWT_MAIL_CONFIRMATION_SECRET_KEY
      ),
      expiresIn: this.config.get(
        Constants.CONFIG_KEY.JWT_MAIL_CONFIRMATION_EXPIRES_IN
      ),
    });
  }

  public async verifyConfirmationToken(token: string) {
    return this.jwtService.verifyAsync(token, {
      secret: this.config.get(
        Constants.CONFIG_KEY.JWT_MAIL_CONFIRMATION_SECRET_KEY
      ),
    });
  }

  public async signForgotPasswordToken(email: string) {
    return this.jwtService.signAsync(
      { email },
      {
        secret: this.config.get(
          Constants.CONFIG_KEY.JWT_MAIL_FORGOT_PASSWORD_SECRET_KEY
        ),
        expiresIn: this.config.get(
          Constants.CONFIG_KEY.JWT_MAIL_FORGOT_PASSWORD_EXPIRES_IN
        ),
      }
    );
  }

  public async verifyForgotPasswordToken(token: string) {
    return this.jwtService.verifyAsync(token, {
      secret: this.config.get(
        Constants.CONFIG_KEY.JWT_MAIL_FORGOT_PASSWORD_SECRET_KEY
      ),
    });
  }
}
