import { FC } from 'react';
import Image from './Image';
import brokenImg from '../../../assets/images/broken.png';
import { Box, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';

const Error: FC = () => {
  const { t } = useTranslation();

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Image
        sx={{
          height: '200px',
        }}
        src={brokenImg}
        alt="Error"
      />
      <Typography variant="h5" sx={{ mt: 2, fontWeight: 'extra-bold' }}>
        {t('common.errors.index')}
      </Typography>
    </Box>
  );
};

export default Error;
