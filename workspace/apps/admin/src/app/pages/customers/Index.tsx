import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';

const CustomersIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('customers.title')} />
      <PageLayout title={t('customers.title')}>Customers</PageLayout>
    </>
  );
};

export default CustomersIndex;
