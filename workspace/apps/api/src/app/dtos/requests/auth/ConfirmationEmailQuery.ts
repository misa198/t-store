import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class ConfirmationEmailQuery {
  @ApiProperty({
    type: String,
    description: MessageHelper.getMessage('auth.confirmationTokens'),
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  token: string;
}
