import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Constants } from '../common/constants/Contants';
import { JwtPayload } from '../common/modals/JwtPayload';

@Injectable()
export class JwtRtStrategy extends PassportStrategy(
  Strategy,
  Constants.JWT_RT_STRATEGY_NAME
) {
  constructor(config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // ignoreExpiration: true,
      secretOrKey: config.get(Constants.CONFIG_KEY.JWT_RT_SECRET_KEY),
    });
  }

  async validate(payload: JwtPayload) {
    return {
      email: payload.email,
      id: payload.id,
      isExpired: false,
    };
  }
}
