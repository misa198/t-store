import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './Order';
import { Status } from './Status';

@Entity({
  name: 'order_status',
})
export class OrderStatus {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @ManyToOne(() => Order, (order) => order.orderStatuses, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'order_id',
  })
  order: Order;

  @ManyToOne(() => Status, (status) => status.orderStatuses, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'status_id',
  })
  status: Status;
}
