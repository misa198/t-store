import { ROLE_CODE } from '@workspace/react/constants';
import { FC } from 'react';
import { Navigate } from 'react-router-dom';
import { useAppSelector } from '../app/hooks/Redux';

const Index: FC = () => {
  const user = useAppSelector((state) => state.auth.user);

  const defaultRoutes = {
    [ROLE_CODE.ADMIN]: '/dashboard',
    [ROLE_CODE.MANAGER]: '/dashboard',
    [ROLE_CODE.CONTENT]: '/posts',
  };

  if (user.data && !user.loading)
    return <Navigate to={defaultRoutes[user.data.role.code]} />;
  // if (!user.data && !user.loading) return <Navigate to="/auth/login" />;
  return <></>;
};

export default Index;
