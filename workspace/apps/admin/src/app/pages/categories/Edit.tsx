import { FC, lazy, useEffect } from 'react';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';
import { useTranslation } from 'react-i18next';
import { useAppDispatch, useAppSelector } from '../../app/hooks/Redux';
import { getCategoryThunk } from '../../app/store/features/category/categoryThunk';
import { useParams } from 'react-router-dom';
import EditCategoryForm from '../../components/pages/categories/EditCategoryForm';

const Error = lazy(() => import('../../components/common/Error'));

const Edit: FC = () => {
  const { slug } = useParams();
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const category = useAppSelector((state) => state.category.category);

  useEffect(() => {
    dispatch(getCategoryThunk(slug as string));
  }, [dispatch, slug]);

  if (category.loading)
    return (
      <>
        <Helmet title={t('categoryEdit.title')} />
        <PageLayout loading />
      </>
    );
  if (category.data) {
    return (
      <>
        <Helmet title={category.data.name} />
        <PageLayout title={category.data.name}>
          <EditCategoryForm category={category.data} />
        </PageLayout>
      </>
    );
  }
  if (category.error)
    return (
      <>
        <Helmet title={t('common.errors.index')} />
        <PageLayout>
          <Error />
        </PageLayout>
      </>
    );
  return <></>;
};

export default Edit;
