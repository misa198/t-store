export enum CategorySortEnum {
  NAME_ASC = 'name_asc',
  NAME_DESC = 'name_desc',
  TIME_ASC = 'time_asc',
  TIME_DESC = 'time_desc',
}
