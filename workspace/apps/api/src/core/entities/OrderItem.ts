import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './Order';
import { Rate } from './Rate';
import { Variant } from './Variant';

@Entity({
  name: 'order_item',
})
export class OrderItem {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'price',
    type: 'float',
    nullable: false,
  })
  price: string;

  @Column({
    name: 'quantity',
    type: 'int',
    nullable: false,
  })
  quantity: number;

  @ManyToOne(() => Variant, (variant) => variant.orderItems, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'variant_id',
  })
  variant: Variant;

  @ManyToOne(() => Order, (order) => order.orderItems, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'order_id',
  })
  order: Order;

  @OneToOne(() => Rate, (rate) => rate.orderItem, {
    cascade: ['insert'],
  })
  rate: Rate;
}
