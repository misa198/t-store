import { ROLE_CODE } from '@workspace/react/constants';
import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode';

const ACCESS_TOKEN_KEY = 'ACCESS_TOKEN';
const REFRESH_TOKEN_KEY = 'REFRESH_TOKEN';

// jwt
export const getAccessToken = () => Cookies.get(ACCESS_TOKEN_KEY);

export const setAccessToken = (token: string) => {
  Cookies.set(ACCESS_TOKEN_KEY, token);
};

export const removeAccessToken = () => {
  Cookies.remove(ACCESS_TOKEN_KEY);
};

export const getRefreshToken = () => Cookies.get(REFRESH_TOKEN_KEY);

export const setRefreshToken = (token: string) => {
  Cookies.set(REFRESH_TOKEN_KEY, token);
};

export const removeRefreshToken = () => {
  Cookies.remove(REFRESH_TOKEN_KEY);
};

// Roles
export const removeAllAuthCookies = () => {
  removeRefreshToken();
  removeAccessToken();
};

export const validRole = (role: string) => {
  return (Object.values(ROLE_CODE) as string[]).includes(role);
};

export const decodeToken = (token: string) => jwtDecode(token);
