import { Image } from './Image';

export interface BrandDetails {
  image?: Image;
  description: string;
  slug: string;
  name: string;
  id: number;
  isActive?: boolean;
}
