import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';
import GridViewOutlinedIcon from '@mui/icons-material/GridViewOutlined';
import GroupsOutlinedIcon from '@mui/icons-material/GroupsOutlined';
import Inventory2OutlinedIcon from '@mui/icons-material/Inventory2Outlined';
import LabelOutlinedIcon from '@mui/icons-material/LabelOutlined';
import ListOutlinedIcon from '@mui/icons-material/ListOutlined';
import LocalMallOutlinedIcon from '@mui/icons-material/LocalMallOutlined';
import MenuIcon from '@mui/icons-material/Menu';
import SettingsInputCompositeOutlinedIcon from '@mui/icons-material/SettingsInputCompositeOutlined';
import { Typography } from '@mui/material';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useTheme } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import useMediaQuery from '@mui/material/useMediaQuery';
import { ROLE_CODE } from '@workspace/react/constants';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import logo from '../../../../assets/images/logo-black.png';
import { useAppDispatch, useAppSelector } from '../../../app/hooks/Redux';
import { toggleDrawer } from '../../../app/store/features/layout/layoutSlice';
import {
  APP_DRAWER_CLOSE_WIDTH,
  APP_DRAWER_WIDTH,
} from '../../../constants/layout';
import Image from '../../common/Image';
import { matchPath } from 'react-router';

const AppDrawer = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const dispatch = useAppDispatch();
  const drawerOpen = useAppSelector((state) => state.layout.drawerOpen);
  const user = useAppSelector((state) => state.auth.user);
  const theme = useTheme();
  const isMdUp = useMediaQuery(theme.breakpoints.up('md'));

  const onToggleDrawer = () => {
    dispatch(toggleDrawer());
  };

  const menuItems = [
    {
      icon: <GridViewOutlinedIcon />,
      label: t('common.drawer.dashboard'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER],
      path: '/dashboard',
    },
    {
      icon: <LabelOutlinedIcon />,
      label: t('common.drawer.brands'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER],
      path: '/brands',
    },
    {
      icon: <ListOutlinedIcon />,
      label: t('common.drawer.categories'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER],
      path: '/categories',
    },
    {
      icon: <Inventory2OutlinedIcon />,
      label: t('common.drawer.products'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER],
      path: '/products',
    },
    {
      icon: <LocalMallOutlinedIcon />,
      label: t('common.drawer.orders'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER],
      path: '/orders',
    },
    {
      icon: <GroupsOutlinedIcon />,
      label: t('common.drawer.customers'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER],
      path: '/customers',
    },
    {
      icon: <AssignmentOutlinedIcon />,
      label: t('common.drawer.posts'),
      roles: [ROLE_CODE.ADMIN, ROLE_CODE.MANAGER, ROLE_CODE.CONTENT],
      path: '/posts',
    },
    {
      icon: <SettingsInputCompositeOutlinedIcon />,
      label: t('common.drawer.settings'),
      roles: [ROLE_CODE.ADMIN],
      path: '/settings',
    },
  ];

  const list = () => (
    <Box role="presentation">
      {user.data && !user.loading && (
        <List>
          {menuItems
            .filter((item) => item.roles.includes(user.data!.role.code))
            .map(({ icon, label, path }) => (
              <Link to={path} key={label}>
                <ListItem
                  disablePadding
                  sx={{ position: 'relative' }}
                  onClick={() => {
                    if (!isMdUp) onToggleDrawer();
                  }}
                >
                  <Box
                    sx={{
                      position: 'absolute',
                      left: 0,
                      top: 0,
                      height: '100%',
                      width: 4,
                      backgroundColor: theme.palette.primary.main,
                      zIndex: 100,
                      borderTopRightRadius: 16,
                      borderBottomRightRadius: 16,
                      display: matchPath(`${path}/*`, location.pathname)
                        ? 'block'
                        : 'none',
                    }}
                  />
                  <ListItemButton>
                    <ListItemIcon
                      sx={{
                        ...(matchPath(`${path}/*`, location.pathname) && {
                          color: theme.palette.primary.main,
                        }),
                      }}
                    >
                      {icon}
                    </ListItemIcon>
                    <ListItemText
                      primary={
                        <Typography
                          sx={{
                            whiteSpace: 'nowrap',
                            fontWeight: '600',
                            ...(matchPath(`${path}/*`, location.pathname) && {
                              color: theme.palette.primary.main,
                            }),
                          }}
                        >
                          {label}
                        </Typography>
                      }
                    />
                  </ListItemButton>
                </ListItem>
              </Link>
            ))}
        </List>
      )}
    </Box>
  );

  return (
    <Drawer
      anchor="left"
      open={isMdUp ? drawerOpen : !drawerOpen}
      onClose={onToggleDrawer}
      variant={isMdUp ? 'permanent' : 'temporary'}
      sx={{
        '.MuiDrawer-paper': {
          transition: theme.transitions.create(['width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
          }),
          overflowX: 'hidden',
          width: APP_DRAWER_WIDTH,
          ...(isMdUp &&
            !drawerOpen && {
              width: `${APP_DRAWER_CLOSE_WIDTH}px !important`,
            }),
        },
      }}
    >
      <Toolbar>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={onToggleDrawer}
          >
            <MenuIcon />
          </IconButton>
          <Image
            sx={{
              height: '20px',
            }}
            src={logo}
            alt="logo"
          />
        </Box>
      </Toolbar>
      <Divider />
      {list()}
    </Drawer>
  );
};

export default AppDrawer;
