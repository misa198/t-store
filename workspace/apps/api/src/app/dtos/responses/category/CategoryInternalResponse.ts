import { AutoMap } from '@automapper/classes';

export class CategoryInternalResponse {
  @AutoMap()
  id: number;

  @AutoMap()
  name: string;

  @AutoMap()
  isActive: boolean;

  @AutoMap()
  slug: string;
}
