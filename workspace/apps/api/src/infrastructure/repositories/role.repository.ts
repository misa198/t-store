import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Role } from '../../core/entities/Role';

@EntityRepository(Role)
export class RoleRepository extends BaseRepository<Role> {}
