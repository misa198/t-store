import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Brand } from '../../core/entities/Brand';

@EntityRepository(Brand)
export class BrandRepository extends BaseRepository<Brand> {}
