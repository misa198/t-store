import { FC, forwardRef, MouseEvent, ReactElement, useState } from 'react';
import {
  Backdrop,
  Box,
  Button,
  Modal,
  Paper,
  Typography,
  useTheme,
} from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';

import {
  animated,
  useSpring,
} from '@react-spring/web/dist/react-spring-web.cjs';
import { useTranslation } from 'react-i18next';

interface FadeProps {
  children?: ReactElement;
  in: boolean;
  onEnter?: () => void;
  onExited?: () => void;
}

const Fade = forwardRef<HTMLDivElement, FadeProps>(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    },
  });

  return (
    <animated.div ref={ref} style={style} {...other}>
      {children}
    </animated.div>
  );
});

interface Props {
  button: FC;
  onClick: (event: MouseEvent<HTMLButtonElement>) => void;
  title: string;
  description: string;
  loading?: boolean;
}

export const ConfirmPopup: FC<Props> = (props) => {
  const { t } = useTranslation();
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      {props.button({
        onClick: () => {
          handleOpen();
        },
      })}
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        open={open}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 150,
        }}
      >
        <Fade in={open}>
          <Paper
            sx={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
              width: {
                xs: 350,
                sm: 500,
              },
              boxShadow: 24,
              p: {
                xs: 3,
                sm: 4,
              },
            }}
          >
            <Typography id="spring-modal-title" variant="h6" component="h2">
              {props.title}
            </Typography>
            <Typography id="spring-modal-description" sx={{ mt: 2 }}>
              {props.description}
            </Typography>
            <Box
              sx={{
                mt: 4,
                display: 'flex',
                justifyContent: 'end',
              }}
            >
              {!props.loading && (
                <Button
                  variant="outlined"
                  color="inherit"
                  onClick={handleClose}
                  sx={{
                    mr: 2,
                  }}
                >
                  {t('common.btn.cancel')}
                </Button>
              )}
              <Button
                variant="contained"
                disableElevation
                onClick={props.onClick}
                disabled={Boolean(props.loading)}
                sx={{
                  backgroundColor: `${theme.palette.primary.main} !important`,
                  color: `${theme.palette.common.white} !important`,
                }}
              >
                {props.loading ? (
                  <CircularProgress
                    size="18px"
                    sx={{
                      color: theme.palette.common.white,
                    }}
                  />
                ) : (
                  t('common.btn.sure')
                )}
              </Button>
            </Box>
          </Paper>
        </Fade>
      </Modal>
    </div>
  );
};
