import { ROLE_CODE } from '@workspace/react/constants';
import { FC, LazyExoticComponent, Suspense } from 'react';
import { Navigate } from 'react-router-dom';
import { useAppSelector } from '../app/hooks/Redux';
import { getAccessToken } from '../utils/auth';

interface Props {
  permissions?: ROLE_CODE[];
  children: LazyExoticComponent<FC>;
}

const accessToken = getAccessToken();

const PrivateRoute: FC<Props> = (props) => {
  const user = useAppSelector((state) => state.auth.user);

  if (!accessToken || user.error) return <Navigate to="/auth/login" />;
  if (!props.permissions)
    return (
      <Suspense fallback={<></>}>
        <props.children />
      </Suspense>
    );
  if (user.loading) return <></>;
  if (!user.loading && user.data) {
    if (props.permissions.includes(user.data.role.code))
      return (
        <Suspense fallback={<></>}>
          <props.children />
        </Suspense>
      );
  }
  return <Navigate to="/403" />;
};

export default PrivateRoute;
