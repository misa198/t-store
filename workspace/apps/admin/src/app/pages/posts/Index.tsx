import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import Helmet from '../../components/common/Helmet';
import PageLayout from '../../components/common/PageLayout';

const PostsIndex: FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet title={t('posts.title')} />
      <PageLayout title={t('posts.title')}>Posts</PageLayout>
    </>
  );
};

export default PostsIndex;
