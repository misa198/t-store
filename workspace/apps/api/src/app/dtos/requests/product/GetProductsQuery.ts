import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsEnum,
  IsInt,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { Constants } from '../../../../core/common/constants/Contants';
import { ProductSortEnum } from '../../../../core/common/enums/ProductSortEnum';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class GetProductsQuery {
  @ApiProperty({
    type: Boolean,
    default: true,
    description: MessageHelper.getMessage('product.isActive'),
    required: false,
    enum: [true, false],
  })
  @Transform(({ value }) => value == 'true')
  @IsOptional()
  @IsBoolean()
  isActive = true;

  @ApiProperty({
    type: String,
    default: ProductSortEnum.TIME_DESC,
    description: MessageHelper.getMessage('product.sortBy'),
    required: false,
    enum: ProductSortEnum,
  })
  @IsOptional()
  @IsEnum(ProductSortEnum)
  sort = ProductSortEnum.TIME_DESC;

  @ApiProperty({
    type: String,
    required: false,
    description: MessageHelper.getMessage('brand.slug'),
  })
  @IsOptional()
  @IsString()
  brand: string;

  @ApiProperty({
    type: String,
    required: false,
    description: MessageHelper.getMessage('category.slug'),
  })
  @IsOptional()
  @IsString()
  categories: string;

  @ApiProperty({
    type: Number,
    required: false,
    description: MessageHelper.getMessage('common.offset'),
    minimum: 0,
  })
  @Transform(({ value }) => Number(value))
  @IsOptional()
  @IsInt()
  @Min(0)
  offset = 0;

  @ApiProperty({
    type: Number,
    required: false,
    description: MessageHelper.getMessage('common.limit'),
    maximum: Constants.MAX_PAGINATION_LIMIT,
    minimum: 0,
  })
  @Transform(({ value }) => Number(value))
  @IsOptional()
  @IsInt()
  @Min(0)
  @Max(Constants.MAX_PAGINATION_LIMIT)
  limit = Constants.DEFAULT_PAGINATION_LIMIT;
}
