import qs from 'query-string';
import { api } from './axios';
import {
  BaseResponse,
  Category,
  CategoryDetails,
  GetCategoriesQuery,
} from '@workspace/react/modals';
import { UpdateCategoryRequest } from '../../modals/request/UpdateCategoryRequest';
import { CreateCategoryRequest } from '../../modals/request/CreateCategoryRequest';

const CATEGORY_URL = '/categories';

export const getCategoriesApi = async (
  params: GetCategoriesQuery
): Promise<BaseResponse<Category[]>> =>
  api.get(
    qs.stringifyUrl({
      url: CATEGORY_URL,
      query: { ...params },
    })
  );

export const getCategoryApi = async (
  slug: string
): Promise<BaseResponse<CategoryDetails>> => api.get(`${CATEGORY_URL}/${slug}`);

export const updateCategoryApi = async (
  slug: string,
  reqBody: UpdateCategoryRequest
): Promise<BaseResponse<CategoryDetails>> =>
  api.put(`${CATEGORY_URL}/${slug}`, reqBody);

export const createCategoryApi = async (
  reqBody: CreateCategoryRequest
): Promise<BaseResponse<CategoryDetails>> => api.post(CATEGORY_URL, reqBody);

export const deleteCategoryApi = async (
  slug: string
): Promise<BaseResponse<void>> => api.delete(`${CATEGORY_URL}/${slug}`);
