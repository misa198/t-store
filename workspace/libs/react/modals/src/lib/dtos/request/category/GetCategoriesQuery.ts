export type GetCategoriesSort = 'name_asc' | 'name_desc' | 'time_asc' | 'time_desc';
export type GetCategoriesIsActive = "false" | 'true'

export interface GetCategoriesQuery {
  isActive?: GetCategoriesIsActive;
  sort?: GetCategoriesSort;
}
