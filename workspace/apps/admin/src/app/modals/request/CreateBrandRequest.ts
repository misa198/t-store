export interface CreateBrandRequest {
  name: string;
  description: string;
}
