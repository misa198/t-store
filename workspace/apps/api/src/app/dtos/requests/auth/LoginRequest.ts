import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';
import {
  EMAIL_MAX_LENGTH,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
} from '@workspace/common/constants';

export class LoginRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.email'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(1, EMAIL_MAX_LENGTH)
  @IsEmail()
  email: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.password'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)
  password: string;
}
