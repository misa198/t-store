import { JwtRtPayload } from './JwtRtPayload';

export interface JwtRtReqPayload extends JwtRtPayload {
  isExpires: boolean;
  email: string;
  id: number;
  refreshToken: string;
}
