import { ApiProperty } from '@nestjs/swagger';
import {
  EMAIL_MAX_LENGTH,
  FIRSTNAME_MAX_LENGTH,
  FIRSTNAME_MIN_LENGTH,
  LASTNAME_MAX_LENGTH,
  LASTNAME_MIN_LENGTH,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
} from '@workspace/common/constants';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';

export class RegisterRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.email'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(1, EMAIL_MAX_LENGTH)
  @IsEmail()
  email: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.password'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)
  password: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.firstName'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(FIRSTNAME_MIN_LENGTH, FIRSTNAME_MAX_LENGTH)
  firstName: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('auth.lastName'),
  })
  @IsString()
  @IsNotEmpty()
  @Length(LASTNAME_MIN_LENGTH, LASTNAME_MAX_LENGTH)
  lastName: string;
}
