import { ChangeEvent, FC } from 'react';
import { PAGINATION_ROWS_PER_PAGE_OPTIONS } from '../../constants/common';
import { TablePagination } from '@mui/material';
import { useTranslation } from 'react-i18next';

interface Props {
  count: number;
  rowsPerPage: number;
  page: number;
  onPageChange: (event: unknown, newPage: number) => void;
  onRowsPerPageChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

const TablePaginationComponent: FC<Props> = ({
  count,
  rowsPerPage,
  page,
  onPageChange,
  onRowsPerPageChange,
}) => {
  const { t } = useTranslation();

  return (
    <TablePagination
      rowsPerPageOptions={PAGINATION_ROWS_PER_PAGE_OPTIONS}
      component="div"
      count={count}
      rowsPerPage={rowsPerPage}
      page={page}
      onPageChange={onPageChange}
      onRowsPerPageChange={onRowsPerPageChange}
      labelDisplayedRows={(params) =>
        t('common.labels.displayedRows', { ...params })
      }
      labelRowsPerPage={t('common.labels.rowsPerPage')}
    />
  );
};

export default TablePaginationComponent;
