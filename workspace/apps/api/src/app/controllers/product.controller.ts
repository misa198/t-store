import { Body, Controller, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RoleEnum } from '../../core/common/enums/RoleEnum';
import { JwtPayload } from '../../core/common/modals/JwtPayload';
import { ReqUser } from '../../core/security/req-user.decorator';
import { Roles } from '../../core/security/roles.decorator';
import { ProductService } from '../../core/services/product.service';
import { CreateProductRequest } from '../dtos/requests/product/CreateProductRequest';
import { GetProductsQuery } from '../dtos/requests/product/GetProductsQuery';
import { UpdateProductRequest } from '../dtos/requests/product/UpdateProductRequest';

@ApiTags('products')
@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post()
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async createProduct(@Body() requestBody: CreateProductRequest) {
    return this.productService.createProduct(requestBody);
  }

  @Get()
  public async getAllBrands(
    @Query() query: GetProductsQuery,
    @ReqUser() reqUser?: JwtPayload
  ) {
    return this.productService.getAllProducts(query, reqUser);
  }

  @Get(':slug')
  public async getProduct(
    @Param('slug') slug,
    @ReqUser() reqUser?: JwtPayload
  ) {
    return this.productService.getProduct(slug, reqUser);
  }

  @Put(':slug')
  @Roles(RoleEnum.ADMIN, RoleEnum.MANAGER)
  public async updateProduct(
    @Param('slug') slug: string,
    @Body() updateProductRequest: UpdateProductRequest
  ) {
    await this.productService.updateProduct(slug, updateProductRequest);
  }
}
