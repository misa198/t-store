import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisService {
  constructor(@Inject(CACHE_MANAGER) private readonly cache: Cache) {}

  async get(key: string): Promise<string | string[]> {
    return new Promise((resolve, reject) => {
      this.cache.get(key, (err, res) => {
        if (err) return reject(err);
        return resolve(res as string | string[]);
      });
    });
  }

  async keys(key: string) {
    return (await this.cache.store.keys(key)) as string[];
  }

  async set(key: string, value: string) {
    return this.cache.set(key, value);
  }

  async del(key: string) {
    await this.cache.del(key);
  }
}
