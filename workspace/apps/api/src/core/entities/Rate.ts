import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { OrderItem } from './OrderItem';

@Entity({
  name: 'rate',
})
export class Rate {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: number;

  @Column({
    name: 'score',
    type: 'int',
    nullable: false,
  })
  score: number;

  @Column({
    name: 'message',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  message: string;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToOne(() => OrderItem, (orderItem) => orderItem.rate, {
    cascade: ['insert'],
  })
  @JoinColumn({
    name: 'order_item_id',
  })
  orderItem: OrderItem;
}
