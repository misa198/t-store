import axios from 'axios';
import { BASE_URL } from '../../constants/api';
import {
  getAccessToken,
  getRefreshToken,
  removeAllAuthCookies,
  setAccessToken,
  setRefreshToken
} from '../../utils/auth';

export const api = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

const handleRefresh = async (refreshToken: string) => {
  api.defaults.headers.common['Authorization'] = `Bearer ${refreshToken}`;
  const REFRESH_API = '/auth/refresh';
  const res = await api.post(REFRESH_API);
  setRefreshToken(res.data.refreshToken);
  setAccessToken(res.data.token);
};

api.interceptors.request.use((config) => {
  if (config.url !== '/auth/refresh') {
    const token = getAccessToken();
    if (token) {
      config.headers = {
        ...config.headers,
        Authorization: `Bearer ${token}`,
      };
    }
  }
  return config;
});

api.interceptors.response.use(
  ({ data }) => data,
  async (error) => {
    const originalRequest = error.config;
    if (originalRequest.url === `/auth/refresh`) {
      removeAllAuthCookies();
      window.location.href = '/auth/login';
      return Promise.reject(error);
    }
    if (error.response.status === 401) {
      if (originalRequest.url !== `/auth/login`) {
        try {
          const refreshToken = getRefreshToken();
          if (refreshToken) {
            await handleRefresh(refreshToken);
            return Promise.resolve(api(originalRequest));
          } else {
            removeAllAuthCookies();
            window.location.href = '/auth/login';
            return Promise.reject(error);
          }
        } catch (e) {
          removeAllAuthCookies();
          window.location.href = '/auth/login';
        }
      }
    }
    if (error.response.status === 404) {
      window.location.replace('/404');
    }
    return Promise.reject(error);
  }
);
