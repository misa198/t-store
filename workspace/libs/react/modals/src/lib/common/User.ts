import { Role } from './Role';

export interface User {
  email: string;
  id: string;
  firstName: string;
  lastName: string;
  role: Role;
}
