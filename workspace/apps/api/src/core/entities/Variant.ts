import { AutoMap } from '@automapper/classes';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { OrderItem } from './OrderItem';
import { Product } from './Product';

@Entity({
  name: 'variant',
})
export class Variant {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  @AutoMap()
  id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 300,
    nullable: false,
  })
  @AutoMap()
  name: string;

  @Column({
    name: 'price',
    type: 'float',
    nullable: false,
  })
  @AutoMap()
  price: number;

  @Column({
    name: 'quantity',
    type: 'int',
    nullable: false,
  })
  @AutoMap()
  quantity: number;

  @Column({
    name: 'is_active',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  @AutoMap()
  isActive: boolean;

  @Column({
    name: 'is_valid',
    type: 'boolean',
    nullable: false,
    default: true,
  })
  @AutoMap()
  isValid: boolean;

  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  @AutoMap()
  createdAt: Date;

  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  @AutoMap()
  updatedAt: Date;

  @ManyToOne(() => Product, (product) => product.variants)
  @JoinColumn({
    name: 'product_id',
  })
  product: Product;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.variant)
  @AutoMap(() => [OrderItem])
  orderItems: OrderItem[];

  public constructor(
    name: string,
    price: number,
    quantity: number,
    product: Product
  ) {
    this.name = name;
    this.price = price;
    this.quantity = quantity;
    this.product = product;
  }
}
