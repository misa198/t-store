import { JwtPayload as DefaultJwtpayload } from 'jsonwebtoken';
import { RoleEnum } from '../enums/RoleEnum';

export interface JwtPayload extends DefaultJwtpayload {
  email: string;
  firstName: string;
  lastName: string;
  id: number;
  profilePicture?: string;
  role: RoleEnum;
}
