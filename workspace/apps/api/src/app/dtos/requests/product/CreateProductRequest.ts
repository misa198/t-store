import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsInt,
  IsNotEmpty,
  IsString,
  Length,
  Min,
  ValidateNested,
} from 'class-validator';
import { MessageHelper } from '../../../../core/common/helpers/MessageHelper';
import { VariantRequest } from './VariantRequest';
import { PRODUCT_NAME_MAX_LENGTH, PRODUCT_SHORT_DESCRIPTION_MAX_LENGTH } from '@workspace/common/constants';

export class CreateProductRequest {
  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('product.name'),
    maxLength: PRODUCT_NAME_MAX_LENGTH,
  })
  @IsString()
  @IsNotEmpty()
  @Length(1, PRODUCT_NAME_MAX_LENGTH)
  name: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('product.shortDescription'),
    maxLength: PRODUCT_SHORT_DESCRIPTION_MAX_LENGTH,
  })
  @IsString()
  @IsNotEmpty()
  @Length(1, PRODUCT_SHORT_DESCRIPTION_MAX_LENGTH)
  shortDescription: string;

  @ApiProperty({
    type: String,
    required: true,
    description: MessageHelper.getMessage('product.description'),
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    type: Number,
    required: true,
    description: MessageHelper.getMessage('product.brandId'),
  })
  @IsInt()
  @Min(1)
  @IsNotEmpty()
  brandId: number;

  @ApiProperty({
    type: [Number],
    required: true,
    description: MessageHelper.getMessage('product.categoryIds'),
  })
  @IsArray()
  @IsInt({ each: true })
  @Min(1, { each: true })
  @ArrayMinSize(1)
  @IsNotEmpty()
  categoryIds: number[];

  @ApiProperty({
    type: VariantRequest,
    required: true,
    description: MessageHelper.getMessage('variant._'),
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => VariantRequest)
  variants: VariantRequest[];
}
