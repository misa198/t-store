import { createAsyncThunk } from '@reduxjs/toolkit';
import { GetCategoriesQuery } from '@workspace/react/modals';
import {
  createCategoryApi,
  getCategoryApi,
  getCategoriesApi,
  updateCategoryApi,
  deleteCategoryApi,
} from '../../../api/categoryApi';
import { toast } from '../toast/toastSlice';
import { UpdateCategoryRequest } from '../../../../modals/request/UpdateCategoryRequest';
import { CreateCategoryRequest } from '../../../../modals/request/CreateCategoryRequest';

export const getCategoriesThunk = createAsyncThunk(
  'category/getCategories',
  async (query: GetCategoriesQuery, { dispatch }) => {
    try {
      const { data } = await getCategoriesApi(query);
      return data;
    } catch (e) {
      dispatch(
        toast({
          variant: 'error',
          message: 'common.errors.index',
        })
      );
      throw e;
    }
  }
);

interface UpdateCategoryThunkPayload {
  slug: string;
  category: UpdateCategoryRequest;
}

export const updateCategoryThunk = createAsyncThunk(
  'category/updateCategory',
  async (payload: UpdateCategoryThunkPayload) => {
    const { data } = await updateCategoryApi(payload.slug, payload.category);
    return data;
  }
);

export const getCategoryThunk = createAsyncThunk(
  'category/getCategory',
  async (slug: string) => {
    const { data } = await getCategoryApi(slug);
    return data;
  }
);

export const createCategoryThunk = createAsyncThunk(
  'category/createCategory',
  async (category: CreateCategoryRequest) => {
    const { data } = await createCategoryApi(category);
    return data;
  }
);

export const deleteCategoryThunk = createAsyncThunk(
  'category/deleteCategory',
  async (slug: string) => {
    await deleteCategoryApi(slug);
    return slug;
  }
);
