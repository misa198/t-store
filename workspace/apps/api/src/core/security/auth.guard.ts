import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard as PassportAuthGuard } from '@nestjs/passport';
import { Constants } from '../common/constants/Contants';

@Injectable()
export class AuthGuard extends PassportAuthGuard(Constants.JWT_STRATEGY_NAME) {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(err, user) {
    if (err || !user)
      throw new UnauthorizedException({
        errorCode: Constants.RESPONSE_EXCEPTION_CODE.INVALID_CREDENTIALS,
      });
    return user;
  }
}
